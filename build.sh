#!/bin/sh
#/***************************************************************************
# *
# * Copyright (c) 2014 Baidu.com, Inc. All Rights Reserved
# *
# **************************************************************************/

#/**
# * @file local_buildlib.sh
# * @author duquanxi@baidu.com
# * @date 2014/07/23/ 14:54:13
# * @version $Revision: 1.0 $
# * @brief
# *
# **/


source $HOME/.bash_profile >/dev/null 2>/dev/null

CURRENT_PATH=`pwd`
ARGSDEV="$2"


LOG_FATAL=1
LOG_WARNING=2
LOG_NOTICE=4
LOG_TRACE=8
LOG_DEBUG=16
LOG_LEVEL_TEXT=(
    [1]="FATAL"
    [2]="WARNING"
    [4]="NOTICE"
    [8]="TRACE"
    [16]="DEBUG"
)

TTY_FATAL=1
TTY_PASS=2
TTY_TRACE=4
TTY_INFO=8
TTY_MODE_TEXT=(
    [1]="[FAIL ]"
    [2]="[PASS ]"
    [4]="[TRACE]"
    [8]=""
)

#0  OFF
#1  高亮显示
#4  underline
#5  闪烁
#7  反白显示
#8  不可见

#30  40  黑色
#31  41  红色
#32  42  绿色
#33  43  黄色
#34  44  蓝色
#35  45  紫红色
#36  46  青蓝色
#37  47  白色
TTY_MODE_COLOR=(
    [1]="1;31"
    [2]="1;32"
    [4]="0;36"
    [8]="1;33"
)

CONF_LOG_FILE="_local_build.log"

##! @BRIEF: print info to tty & log file
##! @IN[int]: $1 => tty mode
##! @IN[string]: $2 => message
##! @RETURN: 0 => sucess; 1 => failure
function Print()
{
    local tty_mode=$1
    local message="$2"

    local time=`date "+%m-%d %H:%M:%S"`
    echo "${LOG_LEVEL_TEXT[$log_level]}: $time: ${MODULE_NAME} * $$ $message" >> ${CONF_LOG_FILE}
    echo -e "\e[${TTY_MODE_COLOR[$tty_mode]}m${TTY_MODE_TEXT[$tty_mode]} ${message}\e[m"
    return $?
}

##! @BRIEF: run cmd if fail exit proc with errno
##! @IN[string]: $@
##! @RETURN: exit with errno
function verify_run()
{
    echo "$@"
    cmd=$@
    $@
    if [ $? -ne 0 ]
    then
        Print $LOG_FATAL "run cmd error: $cmd"
        exit 255
    else
        Print $LOG_NOTICE "run cmd succ: $cmd"
        return 0
    fi
}

##! @BRIEF: make
##! @RETURN:
make()
{
    return 0
    #mvn package -DskipTests
}

##! @BRIEF: unit test run
##! @RETURN:
unittest()
{
    ~/local/maven-3.5/bin/mvn package
}

##! @BRIEF: module test run
##! @RETURN:
moduletest()
{
    return 0
}

##! @BRIEF: sys_init，系统初始化
##! @RETURN:
sys_init()
{
    return 0
}

package_module()
{
    type=$1
    module=$2


    if [ "$type" == "prod" ]
    then
        cd target  && mkdir ${module} && tar -zxvf ${module}_prod.tar.gz -C ${module} && mv ${module}_prod.tar.gz ../ && cd -
    else
      cd target  && mkdir ${module} && tar -zxvf ${module}_${type}.tar.gz  -C ${module} && mv ${module}_${type}.tar.gz ../ && cd -
    fi
}

##! @BRIEF: package，用于打包
##! @RETURN:
package()
{
    type=$1
    source ~/.bash_profile && export JAVA_HOME=$JAVA8_HOME && rm -rf target && mvn -Dmaven.test.skip=true -U clean package ${ARGSDEV} -Denv=$type && \
    package_module $type breeze-percentage-service
}

##! @BRIEF: release，用于打包
##! @RETURN:
release()
{
    return 0
}


print_help()
{
    echo "samples:"
    echo "----------------------------not rewriteable ---------------------------------------------"
    echo "local_build.sh make               :    make + package"
    echo "local_build.sh quick              :    make + package + unittest"
    echo "local_build.sh slow               :    make + package + unittest + moduletest"
    echo "local_build.sh release            :    release"
    echo "-------------------------------rewriteable-----------------------------------------------"
    echo "local_build.sh sys_init           :     安装系统环境需要的软件，如jdk，设置环境 变量"
    echo "local_build.sh make               :     编译"
    echo "local_build.sh unittest           :     执行单元测试"
    echo "local_build.sh moduletest         :     模块测试"
    echo "local_build.sh package            :     打用于上线的tar.gz包"
    echo "local_build.sh release            :     当RB上线时用于拉分支打tag"
    echo "-----------------------------------------------------------------------------------------"
}

Main()
{
    echo $@
    if [ "$1" == "-h" -o "$1" == "--help" -o "$1" == "-help" -o $# -eq 0 ]; then
        print_help
    elif [ "$1" == "quick" ]; then
        make && package && unittest
    elif [ "$1" == "slow" ];then
        make && package && unittest && moduletest
    elif [ "$1" == "release" ];then
        make && package "prod"
    elif [ "$1" == "make" ];then
        make && package "dev" && package "offline"
    elif [ "$1" == "test" ];then
        make && package "test"
    elif [ "$1" == "performance" ];then
        make && package "performance"
    else
        $1
    fi
    return $?
}

Main $@
