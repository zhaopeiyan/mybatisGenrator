package com.ke.tools.finance.dao;

import com.ke.tools.finance.entity.ChangeRecordLog;

public interface ChangeRecordLogMapper {
    int deleteByPrimaryKey(Long id);

    int insert(ChangeRecordLog record);

    int insertSelective(ChangeRecordLog record);

    ChangeRecordLog selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(ChangeRecordLog record);

    int updateByPrimaryKey(ChangeRecordLog record);
}