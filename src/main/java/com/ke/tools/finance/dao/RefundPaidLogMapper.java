package com.ke.tools.finance.dao;

import com.ke.tools.finance.entity.RefundPaidLog;

public interface RefundPaidLogMapper {
    int deleteByPrimaryKey(Long id);

    int insert(RefundPaidLog record);

    int insertSelective(RefundPaidLog record);

    RefundPaidLog selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(RefundPaidLog record);

    int updateByPrimaryKey(RefundPaidLog record);
}