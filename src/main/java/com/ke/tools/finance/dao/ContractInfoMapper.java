package com.ke.tools.finance.dao;

import com.ke.tools.finance.entity.ContractInfo;

public interface ContractInfoMapper {
    int deleteByPrimaryKey(Long id);

    int insert(ContractInfo record);

    int insertSelective(ContractInfo record);

    ContractInfo selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(ContractInfo record);

    int updateByPrimaryKey(ContractInfo record);
}