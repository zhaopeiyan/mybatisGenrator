package com.ke.tools.finance.dao;

import com.ke.tools.finance.entity.RefundReceivableLog;

public interface RefundReceivableLogMapper {
    int deleteByPrimaryKey(Long id);

    int insert(RefundReceivableLog record);

    int insertSelective(RefundReceivableLog record);

    RefundReceivableLog selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(RefundReceivableLog record);

    int updateByPrimaryKey(RefundReceivableLog record);
}