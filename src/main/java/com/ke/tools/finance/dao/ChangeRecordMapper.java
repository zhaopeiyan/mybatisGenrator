package com.ke.tools.finance.dao;

import com.ke.tools.finance.entity.ChangeRecord;

public interface ChangeRecordMapper {
    int deleteByPrimaryKey(Long id);

    int insert(ChangeRecord record);

    int insertSelective(ChangeRecord record);

    ChangeRecord selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(ChangeRecord record);

    int updateByPrimaryKey(ChangeRecord record);
}