package com.ke.tools.finance.dao;

import com.ke.tools.finance.entity.Request;

public interface RequestMapper {
    int deleteByPrimaryKey(Long id);

    int insert(Request record);

    int insertSelective(Request record);

    Request selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(Request record);

    int updateByPrimaryKey(Request record);
}