package com.ke.tools.finance.dao;

import com.ke.tools.finance.entity.SpecialCompanyAccount;
import com.ke.tools.finance.entity.SpecialCompanyAccountManager;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

public interface SpecialCompanyAccountMapper {
    long countByExample(SpecialCompanyAccountManager example);

    int deleteByExample(SpecialCompanyAccountManager example);

    int deleteByPrimaryKey(Long id);

    int insert(SpecialCompanyAccount record);

    int insertSelective(SpecialCompanyAccount record);

    List<SpecialCompanyAccount> selectByExampleWithRowbounds(SpecialCompanyAccountManager example, RowBounds rowBounds);

    List<SpecialCompanyAccount> selectByExample(SpecialCompanyAccountManager example);

    SpecialCompanyAccount selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") SpecialCompanyAccount record, @Param("example") SpecialCompanyAccountManager example);

    int updateByExample(@Param("record") SpecialCompanyAccount record, @Param("example") SpecialCompanyAccountManager example);

    int updateByPrimaryKeySelective(SpecialCompanyAccount record);

    int updateByPrimaryKey(SpecialCompanyAccount record);
}