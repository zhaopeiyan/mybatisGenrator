package com.ke.tools.finance.dao;

import com.ke.tools.finance.entity.Paidup;

public interface PaidupMapper {
    int deleteByPrimaryKey(Long id);

    int insert(Paidup record);

    int insertSelective(Paidup record);

    Paidup selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(Paidup record);

    int updateByPrimaryKey(Paidup record);
}