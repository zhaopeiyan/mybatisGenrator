package com.ke.tools.finance.dao;

import com.ke.tools.finance.entity.BankAccount;

public interface BankAccountMapper {
    int deleteByPrimaryKey(Long id);

    int insert(BankAccount record);

    int insertSelective(BankAccount record);

    BankAccount selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(BankAccount record);

    int updateByPrimaryKey(BankAccount record);
}