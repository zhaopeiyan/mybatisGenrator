package com.ke.tools.finance.common.lock;

/**
 * Created by dongmingbin on 17/6/20.
 */
public interface ILockBiz<T> {

    T doBiz() ;
}
