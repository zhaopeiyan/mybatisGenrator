package com.ke.tools.finance.entity;

import java.math.BigDecimal;
import java.util.Date;

public class Request {
    private Long id;

    private Long contractId;

    private Long receivableId;

    private BigDecimal totalAmount;

    private Date checkStatusTime;

    private Long checkStatusBy;

    private String checkStatus;

    private String memo;

    private Byte status;

    private String dataType;

    private String dataSource;

    private String paidChannel;

    private Date createdTime;

    private Long createdBy;

    private Date updatedTime;

    private Long updatedBy;

    private Long currentFlowId;

    private String checkFailResult;

    private String requestType;

    private Long instanceId;

    private String refundReason;

    private Byte requestStatus;

    private String cancleReason;

    private Long expireTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getContractId() {
        return contractId;
    }

    public void setContractId(Long contractId) {
        this.contractId = contractId;
    }

    public Long getReceivableId() {
        return receivableId;
    }

    public void setReceivableId(Long receivableId) {
        this.receivableId = receivableId;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public Date getCheckStatusTime() {
        return checkStatusTime;
    }

    public void setCheckStatusTime(Date checkStatusTime) {
        this.checkStatusTime = checkStatusTime;
    }

    public Long getCheckStatusBy() {
        return checkStatusBy;
    }

    public void setCheckStatusBy(Long checkStatusBy) {
        this.checkStatusBy = checkStatusBy;
    }

    public String getCheckStatus() {
        return checkStatus;
    }

    public void setCheckStatus(String checkStatus) {
        this.checkStatus = checkStatus;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public Byte getStatus() {
        return status;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }

    public String getDataType() {
        return dataType;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    public String getDataSource() {
        return dataSource;
    }

    public void setDataSource(String dataSource) {
        this.dataSource = dataSource;
    }

    public String getPaidChannel() {
        return paidChannel;
    }

    public void setPaidChannel(String paidChannel) {
        this.paidChannel = paidChannel;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    public Long getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Long createdBy) {
        this.createdBy = createdBy;
    }

    public Date getUpdatedTime() {
        return updatedTime;
    }

    public void setUpdatedTime(Date updatedTime) {
        this.updatedTime = updatedTime;
    }

    public Long getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(Long updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Long getCurrentFlowId() {
        return currentFlowId;
    }

    public void setCurrentFlowId(Long currentFlowId) {
        this.currentFlowId = currentFlowId;
    }

    public String getCheckFailResult() {
        return checkFailResult;
    }

    public void setCheckFailResult(String checkFailResult) {
        this.checkFailResult = checkFailResult;
    }

    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    public Long getInstanceId() {
        return instanceId;
    }

    public void setInstanceId(Long instanceId) {
        this.instanceId = instanceId;
    }

    public String getRefundReason() {
        return refundReason;
    }

    public void setRefundReason(String refundReason) {
        this.refundReason = refundReason;
    }

    public Byte getRequestStatus() {
        return requestStatus;
    }

    public void setRequestStatus(Byte requestStatus) {
        this.requestStatus = requestStatus;
    }

    public String getCancleReason() {
        return cancleReason;
    }

    public void setCancleReason(String cancleReason) {
        this.cancleReason = cancleReason;
    }

    public Long getExpireTime() {
        return expireTime;
    }

    public void setExpireTime(Long expireTime) {
        this.expireTime = expireTime;
    }
}