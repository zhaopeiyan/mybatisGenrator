package com.ke.tools.finance.entity;

import java.util.Date;

public class SpecialCompanyAccount {
    private Long id;

    private Long contractId;

    private String contractNo;

    private String branchCompany;

    private String bizType;

    private String contractData;

    private Integer contractType;

    private Long customerNo;

    private String districtCode;

    private Long houseNo;

    private String itemValue;

    private Long loginId;

    private String signOrgId;

    private Long signUserId;

    private String tradeProduct;

    private Date versionTime;

    private Date createTime;

    private String tranOrgId;

    private Long tranUserId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getContractId() {
        return contractId;
    }

    public void setContractId(Long contractId) {
        this.contractId = contractId;
    }

    public String getContractNo() {
        return contractNo;
    }

    public void setContractNo(String contractNo) {
        this.contractNo = contractNo;
    }

    public String getBranchCompany() {
        return branchCompany;
    }

    public void setBranchCompany(String branchCompany) {
        this.branchCompany = branchCompany;
    }

    public String getBizType() {
        return bizType;
    }

    public void setBizType(String bizType) {
        this.bizType = bizType;
    }

    public String getContractData() {
        return contractData;
    }

    public void setContractData(String contractData) {
        this.contractData = contractData;
    }

    public Integer getContractType() {
        return contractType;
    }

    public void setContractType(Integer contractType) {
        this.contractType = contractType;
    }

    public Long getCustomerNo() {
        return customerNo;
    }

    public void setCustomerNo(Long customerNo) {
        this.customerNo = customerNo;
    }

    public String getDistrictCode() {
        return districtCode;
    }

    public void setDistrictCode(String districtCode) {
        this.districtCode = districtCode;
    }

    public Long getHouseNo() {
        return houseNo;
    }

    public void setHouseNo(Long houseNo) {
        this.houseNo = houseNo;
    }

    public String getItemValue() {
        return itemValue;
    }

    public void setItemValue(String itemValue) {
        this.itemValue = itemValue;
    }

    public Long getLoginId() {
        return loginId;
    }

    public void setLoginId(Long loginId) {
        this.loginId = loginId;
    }

    public String getSignOrgId() {
        return signOrgId;
    }

    public void setSignOrgId(String signOrgId) {
        this.signOrgId = signOrgId;
    }

    public Long getSignUserId() {
        return signUserId;
    }

    public void setSignUserId(Long signUserId) {
        this.signUserId = signUserId;
    }

    public String getTradeProduct() {
        return tradeProduct;
    }

    public void setTradeProduct(String tradeProduct) {
        this.tradeProduct = tradeProduct;
    }

    public Date getVersionTime() {
        return versionTime;
    }

    public void setVersionTime(Date versionTime) {
        this.versionTime = versionTime;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getTranOrgId() {
        return tranOrgId;
    }

    public void setTranOrgId(String tranOrgId) {
        this.tranOrgId = tranOrgId;
    }

    public Long getTranUserId() {
        return tranUserId;
    }

    public void setTranUserId(Long tranUserId) {
        this.tranUserId = tranUserId;
    }
}