package com.ke.tools.finance.entity;

import java.math.BigDecimal;
import java.util.Date;

public class Paidup {
    private Long id;

    private Long requestItemId;

    private Long receivableId;

    private Long contractId;

    private Byte paidType;

    private String paidChannel;

    private String thirdNo;

    private String bankType;

    private Date paidupTime;

    private String payerType;

    private String payer;

    private String payerPhone;

    private String payerAccount;

    private String payerName;

    private String payerBankName;

    private String receiverType;

    private String receiver;

    private String receiverPhone;

    private String receiverAccount;

    private String receiverName;

    private String receiverBankName;

    private BigDecimal totalAmount;

    private String memo;

    private Date checkAccountTime;

    private String checkAccountBy;

    private String checkAccountMemo;

    private String checkAccount;

    private Byte status;

    private String dataType;

    private String dataSource;

    private Date createdTime;

    private Long createdBy;

    private Date updatedTime;

    private Long updatedBy;

    private String receiptNo;

    private String receiverSubBankName;

    private Long requestId;

    private String payerPhoneEnc;

    private String receiverPhoneEnc;

    private Long refundPaidupId;

    private Byte refundWay;

    private String bankCnapsNo;

    private String refundReason;

    private String receiveIdentyNo;

    private String receiveIdentyNoEnc;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getRequestItemId() {
        return requestItemId;
    }

    public void setRequestItemId(Long requestItemId) {
        this.requestItemId = requestItemId;
    }

    public Long getReceivableId() {
        return receivableId;
    }

    public void setReceivableId(Long receivableId) {
        this.receivableId = receivableId;
    }

    public Long getContractId() {
        return contractId;
    }

    public void setContractId(Long contractId) {
        this.contractId = contractId;
    }

    public Byte getPaidType() {
        return paidType;
    }

    public void setPaidType(Byte paidType) {
        this.paidType = paidType;
    }

    public String getPaidChannel() {
        return paidChannel;
    }

    public void setPaidChannel(String paidChannel) {
        this.paidChannel = paidChannel;
    }

    public String getThirdNo() {
        return thirdNo;
    }

    public void setThirdNo(String thirdNo) {
        this.thirdNo = thirdNo;
    }

    public String getBankType() {
        return bankType;
    }

    public void setBankType(String bankType) {
        this.bankType = bankType;
    }

    public Date getPaidupTime() {
        return paidupTime;
    }

    public void setPaidupTime(Date paidupTime) {
        this.paidupTime = paidupTime;
    }

    public String getPayerType() {
        return payerType;
    }

    public void setPayerType(String payerType) {
        this.payerType = payerType;
    }

    public String getPayer() {
        return payer;
    }

    public void setPayer(String payer) {
        this.payer = payer;
    }

    public String getPayerPhone() {
        return payerPhone;
    }

    public void setPayerPhone(String payerPhone) {
        this.payerPhone = payerPhone;
    }

    public String getPayerAccount() {
        return payerAccount;
    }

    public void setPayerAccount(String payerAccount) {
        this.payerAccount = payerAccount;
    }

    public String getPayerName() {
        return payerName;
    }

    public void setPayerName(String payerName) {
        this.payerName = payerName;
    }

    public String getPayerBankName() {
        return payerBankName;
    }

    public void setPayerBankName(String payerBankName) {
        this.payerBankName = payerBankName;
    }

    public String getReceiverType() {
        return receiverType;
    }

    public void setReceiverType(String receiverType) {
        this.receiverType = receiverType;
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public String getReceiverPhone() {
        return receiverPhone;
    }

    public void setReceiverPhone(String receiverPhone) {
        this.receiverPhone = receiverPhone;
    }

    public String getReceiverAccount() {
        return receiverAccount;
    }

    public void setReceiverAccount(String receiverAccount) {
        this.receiverAccount = receiverAccount;
    }

    public String getReceiverName() {
        return receiverName;
    }

    public void setReceiverName(String receiverName) {
        this.receiverName = receiverName;
    }

    public String getReceiverBankName() {
        return receiverBankName;
    }

    public void setReceiverBankName(String receiverBankName) {
        this.receiverBankName = receiverBankName;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public Date getCheckAccountTime() {
        return checkAccountTime;
    }

    public void setCheckAccountTime(Date checkAccountTime) {
        this.checkAccountTime = checkAccountTime;
    }

    public String getCheckAccountBy() {
        return checkAccountBy;
    }

    public void setCheckAccountBy(String checkAccountBy) {
        this.checkAccountBy = checkAccountBy;
    }

    public String getCheckAccountMemo() {
        return checkAccountMemo;
    }

    public void setCheckAccountMemo(String checkAccountMemo) {
        this.checkAccountMemo = checkAccountMemo;
    }

    public String getCheckAccount() {
        return checkAccount;
    }

    public void setCheckAccount(String checkAccount) {
        this.checkAccount = checkAccount;
    }

    public Byte getStatus() {
        return status;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }

    public String getDataType() {
        return dataType;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    public String getDataSource() {
        return dataSource;
    }

    public void setDataSource(String dataSource) {
        this.dataSource = dataSource;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    public Long getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Long createdBy) {
        this.createdBy = createdBy;
    }

    public Date getUpdatedTime() {
        return updatedTime;
    }

    public void setUpdatedTime(Date updatedTime) {
        this.updatedTime = updatedTime;
    }

    public Long getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(Long updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getReceiptNo() {
        return receiptNo;
    }

    public void setReceiptNo(String receiptNo) {
        this.receiptNo = receiptNo;
    }

    public String getReceiverSubBankName() {
        return receiverSubBankName;
    }

    public void setReceiverSubBankName(String receiverSubBankName) {
        this.receiverSubBankName = receiverSubBankName;
    }

    public Long getRequestId() {
        return requestId;
    }

    public void setRequestId(Long requestId) {
        this.requestId = requestId;
    }

    public String getPayerPhoneEnc() {
        return payerPhoneEnc;
    }

    public void setPayerPhoneEnc(String payerPhoneEnc) {
        this.payerPhoneEnc = payerPhoneEnc;
    }

    public String getReceiverPhoneEnc() {
        return receiverPhoneEnc;
    }

    public void setReceiverPhoneEnc(String receiverPhoneEnc) {
        this.receiverPhoneEnc = receiverPhoneEnc;
    }

    public Long getRefundPaidupId() {
        return refundPaidupId;
    }

    public void setRefundPaidupId(Long refundPaidupId) {
        this.refundPaidupId = refundPaidupId;
    }

    public Byte getRefundWay() {
        return refundWay;
    }

    public void setRefundWay(Byte refundWay) {
        this.refundWay = refundWay;
    }

    public String getBankCnapsNo() {
        return bankCnapsNo;
    }

    public void setBankCnapsNo(String bankCnapsNo) {
        this.bankCnapsNo = bankCnapsNo;
    }

    public String getRefundReason() {
        return refundReason;
    }

    public void setRefundReason(String refundReason) {
        this.refundReason = refundReason;
    }

    public String getReceiveIdentyNo() {
        return receiveIdentyNo;
    }

    public void setReceiveIdentyNo(String receiveIdentyNo) {
        this.receiveIdentyNo = receiveIdentyNo;
    }

    public String getReceiveIdentyNoEnc() {
        return receiveIdentyNoEnc;
    }

    public void setReceiveIdentyNoEnc(String receiveIdentyNoEnc) {
        this.receiveIdentyNoEnc = receiveIdentyNoEnc;
    }
}