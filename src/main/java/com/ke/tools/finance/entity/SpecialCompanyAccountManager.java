package com.ke.tools.finance.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class SpecialCompanyAccountManager {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public SpecialCompanyAccountManager() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Long value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Long value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Long value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Long value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Long value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Long value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Long> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Long> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Long value1, Long value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Long value1, Long value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andContractIdIsNull() {
            addCriterion("contract_id is null");
            return (Criteria) this;
        }

        public Criteria andContractIdIsNotNull() {
            addCriterion("contract_id is not null");
            return (Criteria) this;
        }

        public Criteria andContractIdEqualTo(Long value) {
            addCriterion("contract_id =", value, "contractId");
            return (Criteria) this;
        }

        public Criteria andContractIdNotEqualTo(Long value) {
            addCriterion("contract_id <>", value, "contractId");
            return (Criteria) this;
        }

        public Criteria andContractIdGreaterThan(Long value) {
            addCriterion("contract_id >", value, "contractId");
            return (Criteria) this;
        }

        public Criteria andContractIdGreaterThanOrEqualTo(Long value) {
            addCriterion("contract_id >=", value, "contractId");
            return (Criteria) this;
        }

        public Criteria andContractIdLessThan(Long value) {
            addCriterion("contract_id <", value, "contractId");
            return (Criteria) this;
        }

        public Criteria andContractIdLessThanOrEqualTo(Long value) {
            addCriterion("contract_id <=", value, "contractId");
            return (Criteria) this;
        }

        public Criteria andContractIdIn(List<Long> values) {
            addCriterion("contract_id in", values, "contractId");
            return (Criteria) this;
        }

        public Criteria andContractIdNotIn(List<Long> values) {
            addCriterion("contract_id not in", values, "contractId");
            return (Criteria) this;
        }

        public Criteria andContractIdBetween(Long value1, Long value2) {
            addCriterion("contract_id between", value1, value2, "contractId");
            return (Criteria) this;
        }

        public Criteria andContractIdNotBetween(Long value1, Long value2) {
            addCriterion("contract_id not between", value1, value2, "contractId");
            return (Criteria) this;
        }

        public Criteria andContractNoIsNull() {
            addCriterion("contract_no is null");
            return (Criteria) this;
        }

        public Criteria andContractNoIsNotNull() {
            addCriterion("contract_no is not null");
            return (Criteria) this;
        }

        public Criteria andContractNoEqualTo(String value) {
            addCriterion("contract_no =", value, "contractNo");
            return (Criteria) this;
        }

        public Criteria andContractNoNotEqualTo(String value) {
            addCriterion("contract_no <>", value, "contractNo");
            return (Criteria) this;
        }

        public Criteria andContractNoGreaterThan(String value) {
            addCriterion("contract_no >", value, "contractNo");
            return (Criteria) this;
        }

        public Criteria andContractNoGreaterThanOrEqualTo(String value) {
            addCriterion("contract_no >=", value, "contractNo");
            return (Criteria) this;
        }

        public Criteria andContractNoLessThan(String value) {
            addCriterion("contract_no <", value, "contractNo");
            return (Criteria) this;
        }

        public Criteria andContractNoLessThanOrEqualTo(String value) {
            addCriterion("contract_no <=", value, "contractNo");
            return (Criteria) this;
        }

        public Criteria andContractNoLike(String value) {
            addCriterion("contract_no like", value, "contractNo");
            return (Criteria) this;
        }

        public Criteria andContractNoNotLike(String value) {
            addCriterion("contract_no not like", value, "contractNo");
            return (Criteria) this;
        }

        public Criteria andContractNoIn(List<String> values) {
            addCriterion("contract_no in", values, "contractNo");
            return (Criteria) this;
        }

        public Criteria andContractNoNotIn(List<String> values) {
            addCriterion("contract_no not in", values, "contractNo");
            return (Criteria) this;
        }

        public Criteria andContractNoBetween(String value1, String value2) {
            addCriterion("contract_no between", value1, value2, "contractNo");
            return (Criteria) this;
        }

        public Criteria andContractNoNotBetween(String value1, String value2) {
            addCriterion("contract_no not between", value1, value2, "contractNo");
            return (Criteria) this;
        }

        public Criteria andBranchCompanyIsNull() {
            addCriterion("branch_company is null");
            return (Criteria) this;
        }

        public Criteria andBranchCompanyIsNotNull() {
            addCriterion("branch_company is not null");
            return (Criteria) this;
        }

        public Criteria andBranchCompanyEqualTo(String value) {
            addCriterion("branch_company =", value, "branchCompany");
            return (Criteria) this;
        }

        public Criteria andBranchCompanyNotEqualTo(String value) {
            addCriterion("branch_company <>", value, "branchCompany");
            return (Criteria) this;
        }

        public Criteria andBranchCompanyGreaterThan(String value) {
            addCriterion("branch_company >", value, "branchCompany");
            return (Criteria) this;
        }

        public Criteria andBranchCompanyGreaterThanOrEqualTo(String value) {
            addCriterion("branch_company >=", value, "branchCompany");
            return (Criteria) this;
        }

        public Criteria andBranchCompanyLessThan(String value) {
            addCriterion("branch_company <", value, "branchCompany");
            return (Criteria) this;
        }

        public Criteria andBranchCompanyLessThanOrEqualTo(String value) {
            addCriterion("branch_company <=", value, "branchCompany");
            return (Criteria) this;
        }

        public Criteria andBranchCompanyLike(String value) {
            addCriterion("branch_company like", value, "branchCompany");
            return (Criteria) this;
        }

        public Criteria andBranchCompanyNotLike(String value) {
            addCriterion("branch_company not like", value, "branchCompany");
            return (Criteria) this;
        }

        public Criteria andBranchCompanyIn(List<String> values) {
            addCriterion("branch_company in", values, "branchCompany");
            return (Criteria) this;
        }

        public Criteria andBranchCompanyNotIn(List<String> values) {
            addCriterion("branch_company not in", values, "branchCompany");
            return (Criteria) this;
        }

        public Criteria andBranchCompanyBetween(String value1, String value2) {
            addCriterion("branch_company between", value1, value2, "branchCompany");
            return (Criteria) this;
        }

        public Criteria andBranchCompanyNotBetween(String value1, String value2) {
            addCriterion("branch_company not between", value1, value2, "branchCompany");
            return (Criteria) this;
        }

        public Criteria andBizTypeIsNull() {
            addCriterion("biz_type is null");
            return (Criteria) this;
        }

        public Criteria andBizTypeIsNotNull() {
            addCriterion("biz_type is not null");
            return (Criteria) this;
        }

        public Criteria andBizTypeEqualTo(String value) {
            addCriterion("biz_type =", value, "bizType");
            return (Criteria) this;
        }

        public Criteria andBizTypeNotEqualTo(String value) {
            addCriterion("biz_type <>", value, "bizType");
            return (Criteria) this;
        }

        public Criteria andBizTypeGreaterThan(String value) {
            addCriterion("biz_type >", value, "bizType");
            return (Criteria) this;
        }

        public Criteria andBizTypeGreaterThanOrEqualTo(String value) {
            addCriterion("biz_type >=", value, "bizType");
            return (Criteria) this;
        }

        public Criteria andBizTypeLessThan(String value) {
            addCriterion("biz_type <", value, "bizType");
            return (Criteria) this;
        }

        public Criteria andBizTypeLessThanOrEqualTo(String value) {
            addCriterion("biz_type <=", value, "bizType");
            return (Criteria) this;
        }

        public Criteria andBizTypeLike(String value) {
            addCriterion("biz_type like", value, "bizType");
            return (Criteria) this;
        }

        public Criteria andBizTypeNotLike(String value) {
            addCriterion("biz_type not like", value, "bizType");
            return (Criteria) this;
        }

        public Criteria andBizTypeIn(List<String> values) {
            addCriterion("biz_type in", values, "bizType");
            return (Criteria) this;
        }

        public Criteria andBizTypeNotIn(List<String> values) {
            addCriterion("biz_type not in", values, "bizType");
            return (Criteria) this;
        }

        public Criteria andBizTypeBetween(String value1, String value2) {
            addCriterion("biz_type between", value1, value2, "bizType");
            return (Criteria) this;
        }

        public Criteria andBizTypeNotBetween(String value1, String value2) {
            addCriterion("biz_type not between", value1, value2, "bizType");
            return (Criteria) this;
        }

        public Criteria andContractDataIsNull() {
            addCriterion("contract_data is null");
            return (Criteria) this;
        }

        public Criteria andContractDataIsNotNull() {
            addCriterion("contract_data is not null");
            return (Criteria) this;
        }

        public Criteria andContractDataEqualTo(String value) {
            addCriterion("contract_data =", value, "contractData");
            return (Criteria) this;
        }

        public Criteria andContractDataNotEqualTo(String value) {
            addCriterion("contract_data <>", value, "contractData");
            return (Criteria) this;
        }

        public Criteria andContractDataGreaterThan(String value) {
            addCriterion("contract_data >", value, "contractData");
            return (Criteria) this;
        }

        public Criteria andContractDataGreaterThanOrEqualTo(String value) {
            addCriterion("contract_data >=", value, "contractData");
            return (Criteria) this;
        }

        public Criteria andContractDataLessThan(String value) {
            addCriterion("contract_data <", value, "contractData");
            return (Criteria) this;
        }

        public Criteria andContractDataLessThanOrEqualTo(String value) {
            addCriterion("contract_data <=", value, "contractData");
            return (Criteria) this;
        }

        public Criteria andContractDataLike(String value) {
            addCriterion("contract_data like", value, "contractData");
            return (Criteria) this;
        }

        public Criteria andContractDataNotLike(String value) {
            addCriterion("contract_data not like", value, "contractData");
            return (Criteria) this;
        }

        public Criteria andContractDataIn(List<String> values) {
            addCriterion("contract_data in", values, "contractData");
            return (Criteria) this;
        }

        public Criteria andContractDataNotIn(List<String> values) {
            addCriterion("contract_data not in", values, "contractData");
            return (Criteria) this;
        }

        public Criteria andContractDataBetween(String value1, String value2) {
            addCriterion("contract_data between", value1, value2, "contractData");
            return (Criteria) this;
        }

        public Criteria andContractDataNotBetween(String value1, String value2) {
            addCriterion("contract_data not between", value1, value2, "contractData");
            return (Criteria) this;
        }

        public Criteria andContractTypeIsNull() {
            addCriterion("contract_type is null");
            return (Criteria) this;
        }

        public Criteria andContractTypeIsNotNull() {
            addCriterion("contract_type is not null");
            return (Criteria) this;
        }

        public Criteria andContractTypeEqualTo(Integer value) {
            addCriterion("contract_type =", value, "contractType");
            return (Criteria) this;
        }

        public Criteria andContractTypeNotEqualTo(Integer value) {
            addCriterion("contract_type <>", value, "contractType");
            return (Criteria) this;
        }

        public Criteria andContractTypeGreaterThan(Integer value) {
            addCriterion("contract_type >", value, "contractType");
            return (Criteria) this;
        }

        public Criteria andContractTypeGreaterThanOrEqualTo(Integer value) {
            addCriterion("contract_type >=", value, "contractType");
            return (Criteria) this;
        }

        public Criteria andContractTypeLessThan(Integer value) {
            addCriterion("contract_type <", value, "contractType");
            return (Criteria) this;
        }

        public Criteria andContractTypeLessThanOrEqualTo(Integer value) {
            addCriterion("contract_type <=", value, "contractType");
            return (Criteria) this;
        }

        public Criteria andContractTypeIn(List<Integer> values) {
            addCriterion("contract_type in", values, "contractType");
            return (Criteria) this;
        }

        public Criteria andContractTypeNotIn(List<Integer> values) {
            addCriterion("contract_type not in", values, "contractType");
            return (Criteria) this;
        }

        public Criteria andContractTypeBetween(Integer value1, Integer value2) {
            addCriterion("contract_type between", value1, value2, "contractType");
            return (Criteria) this;
        }

        public Criteria andContractTypeNotBetween(Integer value1, Integer value2) {
            addCriterion("contract_type not between", value1, value2, "contractType");
            return (Criteria) this;
        }

        public Criteria andCustomerNoIsNull() {
            addCriterion("customer_no is null");
            return (Criteria) this;
        }

        public Criteria andCustomerNoIsNotNull() {
            addCriterion("customer_no is not null");
            return (Criteria) this;
        }

        public Criteria andCustomerNoEqualTo(Long value) {
            addCriterion("customer_no =", value, "customerNo");
            return (Criteria) this;
        }

        public Criteria andCustomerNoNotEqualTo(Long value) {
            addCriterion("customer_no <>", value, "customerNo");
            return (Criteria) this;
        }

        public Criteria andCustomerNoGreaterThan(Long value) {
            addCriterion("customer_no >", value, "customerNo");
            return (Criteria) this;
        }

        public Criteria andCustomerNoGreaterThanOrEqualTo(Long value) {
            addCriterion("customer_no >=", value, "customerNo");
            return (Criteria) this;
        }

        public Criteria andCustomerNoLessThan(Long value) {
            addCriterion("customer_no <", value, "customerNo");
            return (Criteria) this;
        }

        public Criteria andCustomerNoLessThanOrEqualTo(Long value) {
            addCriterion("customer_no <=", value, "customerNo");
            return (Criteria) this;
        }

        public Criteria andCustomerNoIn(List<Long> values) {
            addCriterion("customer_no in", values, "customerNo");
            return (Criteria) this;
        }

        public Criteria andCustomerNoNotIn(List<Long> values) {
            addCriterion("customer_no not in", values, "customerNo");
            return (Criteria) this;
        }

        public Criteria andCustomerNoBetween(Long value1, Long value2) {
            addCriterion("customer_no between", value1, value2, "customerNo");
            return (Criteria) this;
        }

        public Criteria andCustomerNoNotBetween(Long value1, Long value2) {
            addCriterion("customer_no not between", value1, value2, "customerNo");
            return (Criteria) this;
        }

        public Criteria andDistrictCodeIsNull() {
            addCriterion("district_code is null");
            return (Criteria) this;
        }

        public Criteria andDistrictCodeIsNotNull() {
            addCriterion("district_code is not null");
            return (Criteria) this;
        }

        public Criteria andDistrictCodeEqualTo(String value) {
            addCriterion("district_code =", value, "districtCode");
            return (Criteria) this;
        }

        public Criteria andDistrictCodeNotEqualTo(String value) {
            addCriterion("district_code <>", value, "districtCode");
            return (Criteria) this;
        }

        public Criteria andDistrictCodeGreaterThan(String value) {
            addCriterion("district_code >", value, "districtCode");
            return (Criteria) this;
        }

        public Criteria andDistrictCodeGreaterThanOrEqualTo(String value) {
            addCriterion("district_code >=", value, "districtCode");
            return (Criteria) this;
        }

        public Criteria andDistrictCodeLessThan(String value) {
            addCriterion("district_code <", value, "districtCode");
            return (Criteria) this;
        }

        public Criteria andDistrictCodeLessThanOrEqualTo(String value) {
            addCriterion("district_code <=", value, "districtCode");
            return (Criteria) this;
        }

        public Criteria andDistrictCodeLike(String value) {
            addCriterion("district_code like", value, "districtCode");
            return (Criteria) this;
        }

        public Criteria andDistrictCodeNotLike(String value) {
            addCriterion("district_code not like", value, "districtCode");
            return (Criteria) this;
        }

        public Criteria andDistrictCodeIn(List<String> values) {
            addCriterion("district_code in", values, "districtCode");
            return (Criteria) this;
        }

        public Criteria andDistrictCodeNotIn(List<String> values) {
            addCriterion("district_code not in", values, "districtCode");
            return (Criteria) this;
        }

        public Criteria andDistrictCodeBetween(String value1, String value2) {
            addCriterion("district_code between", value1, value2, "districtCode");
            return (Criteria) this;
        }

        public Criteria andDistrictCodeNotBetween(String value1, String value2) {
            addCriterion("district_code not between", value1, value2, "districtCode");
            return (Criteria) this;
        }

        public Criteria andHouseNoIsNull() {
            addCriterion("house_no is null");
            return (Criteria) this;
        }

        public Criteria andHouseNoIsNotNull() {
            addCriterion("house_no is not null");
            return (Criteria) this;
        }

        public Criteria andHouseNoEqualTo(Long value) {
            addCriterion("house_no =", value, "houseNo");
            return (Criteria) this;
        }

        public Criteria andHouseNoNotEqualTo(Long value) {
            addCriterion("house_no <>", value, "houseNo");
            return (Criteria) this;
        }

        public Criteria andHouseNoGreaterThan(Long value) {
            addCriterion("house_no >", value, "houseNo");
            return (Criteria) this;
        }

        public Criteria andHouseNoGreaterThanOrEqualTo(Long value) {
            addCriterion("house_no >=", value, "houseNo");
            return (Criteria) this;
        }

        public Criteria andHouseNoLessThan(Long value) {
            addCriterion("house_no <", value, "houseNo");
            return (Criteria) this;
        }

        public Criteria andHouseNoLessThanOrEqualTo(Long value) {
            addCriterion("house_no <=", value, "houseNo");
            return (Criteria) this;
        }

        public Criteria andHouseNoIn(List<Long> values) {
            addCriterion("house_no in", values, "houseNo");
            return (Criteria) this;
        }

        public Criteria andHouseNoNotIn(List<Long> values) {
            addCriterion("house_no not in", values, "houseNo");
            return (Criteria) this;
        }

        public Criteria andHouseNoBetween(Long value1, Long value2) {
            addCriterion("house_no between", value1, value2, "houseNo");
            return (Criteria) this;
        }

        public Criteria andHouseNoNotBetween(Long value1, Long value2) {
            addCriterion("house_no not between", value1, value2, "houseNo");
            return (Criteria) this;
        }

        public Criteria andItemValueIsNull() {
            addCriterion("item_value is null");
            return (Criteria) this;
        }

        public Criteria andItemValueIsNotNull() {
            addCriterion("item_value is not null");
            return (Criteria) this;
        }

        public Criteria andItemValueEqualTo(String value) {
            addCriterion("item_value =", value, "itemValue");
            return (Criteria) this;
        }

        public Criteria andItemValueNotEqualTo(String value) {
            addCriterion("item_value <>", value, "itemValue");
            return (Criteria) this;
        }

        public Criteria andItemValueGreaterThan(String value) {
            addCriterion("item_value >", value, "itemValue");
            return (Criteria) this;
        }

        public Criteria andItemValueGreaterThanOrEqualTo(String value) {
            addCriterion("item_value >=", value, "itemValue");
            return (Criteria) this;
        }

        public Criteria andItemValueLessThan(String value) {
            addCriterion("item_value <", value, "itemValue");
            return (Criteria) this;
        }

        public Criteria andItemValueLessThanOrEqualTo(String value) {
            addCriterion("item_value <=", value, "itemValue");
            return (Criteria) this;
        }

        public Criteria andItemValueLike(String value) {
            addCriterion("item_value like", value, "itemValue");
            return (Criteria) this;
        }

        public Criteria andItemValueNotLike(String value) {
            addCriterion("item_value not like", value, "itemValue");
            return (Criteria) this;
        }

        public Criteria andItemValueIn(List<String> values) {
            addCriterion("item_value in", values, "itemValue");
            return (Criteria) this;
        }

        public Criteria andItemValueNotIn(List<String> values) {
            addCriterion("item_value not in", values, "itemValue");
            return (Criteria) this;
        }

        public Criteria andItemValueBetween(String value1, String value2) {
            addCriterion("item_value between", value1, value2, "itemValue");
            return (Criteria) this;
        }

        public Criteria andItemValueNotBetween(String value1, String value2) {
            addCriterion("item_value not between", value1, value2, "itemValue");
            return (Criteria) this;
        }

        public Criteria andLoginIdIsNull() {
            addCriterion("login_id is null");
            return (Criteria) this;
        }

        public Criteria andLoginIdIsNotNull() {
            addCriterion("login_id is not null");
            return (Criteria) this;
        }

        public Criteria andLoginIdEqualTo(Long value) {
            addCriterion("login_id =", value, "loginId");
            return (Criteria) this;
        }

        public Criteria andLoginIdNotEqualTo(Long value) {
            addCriterion("login_id <>", value, "loginId");
            return (Criteria) this;
        }

        public Criteria andLoginIdGreaterThan(Long value) {
            addCriterion("login_id >", value, "loginId");
            return (Criteria) this;
        }

        public Criteria andLoginIdGreaterThanOrEqualTo(Long value) {
            addCriterion("login_id >=", value, "loginId");
            return (Criteria) this;
        }

        public Criteria andLoginIdLessThan(Long value) {
            addCriterion("login_id <", value, "loginId");
            return (Criteria) this;
        }

        public Criteria andLoginIdLessThanOrEqualTo(Long value) {
            addCriterion("login_id <=", value, "loginId");
            return (Criteria) this;
        }

        public Criteria andLoginIdIn(List<Long> values) {
            addCriterion("login_id in", values, "loginId");
            return (Criteria) this;
        }

        public Criteria andLoginIdNotIn(List<Long> values) {
            addCriterion("login_id not in", values, "loginId");
            return (Criteria) this;
        }

        public Criteria andLoginIdBetween(Long value1, Long value2) {
            addCriterion("login_id between", value1, value2, "loginId");
            return (Criteria) this;
        }

        public Criteria andLoginIdNotBetween(Long value1, Long value2) {
            addCriterion("login_id not between", value1, value2, "loginId");
            return (Criteria) this;
        }

        public Criteria andSignOrgIdIsNull() {
            addCriterion("sign_org_id is null");
            return (Criteria) this;
        }

        public Criteria andSignOrgIdIsNotNull() {
            addCriterion("sign_org_id is not null");
            return (Criteria) this;
        }

        public Criteria andSignOrgIdEqualTo(String value) {
            addCriterion("sign_org_id =", value, "signOrgId");
            return (Criteria) this;
        }

        public Criteria andSignOrgIdNotEqualTo(String value) {
            addCriterion("sign_org_id <>", value, "signOrgId");
            return (Criteria) this;
        }

        public Criteria andSignOrgIdGreaterThan(String value) {
            addCriterion("sign_org_id >", value, "signOrgId");
            return (Criteria) this;
        }

        public Criteria andSignOrgIdGreaterThanOrEqualTo(String value) {
            addCriterion("sign_org_id >=", value, "signOrgId");
            return (Criteria) this;
        }

        public Criteria andSignOrgIdLessThan(String value) {
            addCriterion("sign_org_id <", value, "signOrgId");
            return (Criteria) this;
        }

        public Criteria andSignOrgIdLessThanOrEqualTo(String value) {
            addCriterion("sign_org_id <=", value, "signOrgId");
            return (Criteria) this;
        }

        public Criteria andSignOrgIdLike(String value) {
            addCriterion("sign_org_id like", value, "signOrgId");
            return (Criteria) this;
        }

        public Criteria andSignOrgIdNotLike(String value) {
            addCriterion("sign_org_id not like", value, "signOrgId");
            return (Criteria) this;
        }

        public Criteria andSignOrgIdIn(List<String> values) {
            addCriterion("sign_org_id in", values, "signOrgId");
            return (Criteria) this;
        }

        public Criteria andSignOrgIdNotIn(List<String> values) {
            addCriterion("sign_org_id not in", values, "signOrgId");
            return (Criteria) this;
        }

        public Criteria andSignOrgIdBetween(String value1, String value2) {
            addCriterion("sign_org_id between", value1, value2, "signOrgId");
            return (Criteria) this;
        }

        public Criteria andSignOrgIdNotBetween(String value1, String value2) {
            addCriterion("sign_org_id not between", value1, value2, "signOrgId");
            return (Criteria) this;
        }

        public Criteria andSignUserIdIsNull() {
            addCriterion("sign_user_id is null");
            return (Criteria) this;
        }

        public Criteria andSignUserIdIsNotNull() {
            addCriterion("sign_user_id is not null");
            return (Criteria) this;
        }

        public Criteria andSignUserIdEqualTo(Long value) {
            addCriterion("sign_user_id =", value, "signUserId");
            return (Criteria) this;
        }

        public Criteria andSignUserIdNotEqualTo(Long value) {
            addCriterion("sign_user_id <>", value, "signUserId");
            return (Criteria) this;
        }

        public Criteria andSignUserIdGreaterThan(Long value) {
            addCriterion("sign_user_id >", value, "signUserId");
            return (Criteria) this;
        }

        public Criteria andSignUserIdGreaterThanOrEqualTo(Long value) {
            addCriterion("sign_user_id >=", value, "signUserId");
            return (Criteria) this;
        }

        public Criteria andSignUserIdLessThan(Long value) {
            addCriterion("sign_user_id <", value, "signUserId");
            return (Criteria) this;
        }

        public Criteria andSignUserIdLessThanOrEqualTo(Long value) {
            addCriterion("sign_user_id <=", value, "signUserId");
            return (Criteria) this;
        }

        public Criteria andSignUserIdIn(List<Long> values) {
            addCriterion("sign_user_id in", values, "signUserId");
            return (Criteria) this;
        }

        public Criteria andSignUserIdNotIn(List<Long> values) {
            addCriterion("sign_user_id not in", values, "signUserId");
            return (Criteria) this;
        }

        public Criteria andSignUserIdBetween(Long value1, Long value2) {
            addCriterion("sign_user_id between", value1, value2, "signUserId");
            return (Criteria) this;
        }

        public Criteria andSignUserIdNotBetween(Long value1, Long value2) {
            addCriterion("sign_user_id not between", value1, value2, "signUserId");
            return (Criteria) this;
        }

        public Criteria andTradeProductIsNull() {
            addCriterion("trade_product is null");
            return (Criteria) this;
        }

        public Criteria andTradeProductIsNotNull() {
            addCriterion("trade_product is not null");
            return (Criteria) this;
        }

        public Criteria andTradeProductEqualTo(String value) {
            addCriterion("trade_product =", value, "tradeProduct");
            return (Criteria) this;
        }

        public Criteria andTradeProductNotEqualTo(String value) {
            addCriterion("trade_product <>", value, "tradeProduct");
            return (Criteria) this;
        }

        public Criteria andTradeProductGreaterThan(String value) {
            addCriterion("trade_product >", value, "tradeProduct");
            return (Criteria) this;
        }

        public Criteria andTradeProductGreaterThanOrEqualTo(String value) {
            addCriterion("trade_product >=", value, "tradeProduct");
            return (Criteria) this;
        }

        public Criteria andTradeProductLessThan(String value) {
            addCriterion("trade_product <", value, "tradeProduct");
            return (Criteria) this;
        }

        public Criteria andTradeProductLessThanOrEqualTo(String value) {
            addCriterion("trade_product <=", value, "tradeProduct");
            return (Criteria) this;
        }

        public Criteria andTradeProductLike(String value) {
            addCriterion("trade_product like", value, "tradeProduct");
            return (Criteria) this;
        }

        public Criteria andTradeProductNotLike(String value) {
            addCriterion("trade_product not like", value, "tradeProduct");
            return (Criteria) this;
        }

        public Criteria andTradeProductIn(List<String> values) {
            addCriterion("trade_product in", values, "tradeProduct");
            return (Criteria) this;
        }

        public Criteria andTradeProductNotIn(List<String> values) {
            addCriterion("trade_product not in", values, "tradeProduct");
            return (Criteria) this;
        }

        public Criteria andTradeProductBetween(String value1, String value2) {
            addCriterion("trade_product between", value1, value2, "tradeProduct");
            return (Criteria) this;
        }

        public Criteria andTradeProductNotBetween(String value1, String value2) {
            addCriterion("trade_product not between", value1, value2, "tradeProduct");
            return (Criteria) this;
        }

        public Criteria andVersionTimeIsNull() {
            addCriterion("version_time is null");
            return (Criteria) this;
        }

        public Criteria andVersionTimeIsNotNull() {
            addCriterion("version_time is not null");
            return (Criteria) this;
        }

        public Criteria andVersionTimeEqualTo(Date value) {
            addCriterion("version_time =", value, "versionTime");
            return (Criteria) this;
        }

        public Criteria andVersionTimeNotEqualTo(Date value) {
            addCriterion("version_time <>", value, "versionTime");
            return (Criteria) this;
        }

        public Criteria andVersionTimeGreaterThan(Date value) {
            addCriterion("version_time >", value, "versionTime");
            return (Criteria) this;
        }

        public Criteria andVersionTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("version_time >=", value, "versionTime");
            return (Criteria) this;
        }

        public Criteria andVersionTimeLessThan(Date value) {
            addCriterion("version_time <", value, "versionTime");
            return (Criteria) this;
        }

        public Criteria andVersionTimeLessThanOrEqualTo(Date value) {
            addCriterion("version_time <=", value, "versionTime");
            return (Criteria) this;
        }

        public Criteria andVersionTimeIn(List<Date> values) {
            addCriterion("version_time in", values, "versionTime");
            return (Criteria) this;
        }

        public Criteria andVersionTimeNotIn(List<Date> values) {
            addCriterion("version_time not in", values, "versionTime");
            return (Criteria) this;
        }

        public Criteria andVersionTimeBetween(Date value1, Date value2) {
            addCriterion("version_time between", value1, value2, "versionTime");
            return (Criteria) this;
        }

        public Criteria andVersionTimeNotBetween(Date value1, Date value2) {
            addCriterion("version_time not between", value1, value2, "versionTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNull() {
            addCriterion("create_time is null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNotNull() {
            addCriterion("create_time is not null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeEqualTo(Date value) {
            addCriterion("create_time =", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotEqualTo(Date value) {
            addCriterion("create_time <>", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThan(Date value) {
            addCriterion("create_time >", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("create_time >=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThan(Date value) {
            addCriterion("create_time <", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("create_time <=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIn(List<Date> values) {
            addCriterion("create_time in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotIn(List<Date> values) {
            addCriterion("create_time not in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeBetween(Date value1, Date value2) {
            addCriterion("create_time between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("create_time not between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andTranOrgIdIsNull() {
            addCriterion("tran_org_id is null");
            return (Criteria) this;
        }

        public Criteria andTranOrgIdIsNotNull() {
            addCriterion("tran_org_id is not null");
            return (Criteria) this;
        }

        public Criteria andTranOrgIdEqualTo(String value) {
            addCriterion("tran_org_id =", value, "tranOrgId");
            return (Criteria) this;
        }

        public Criteria andTranOrgIdNotEqualTo(String value) {
            addCriterion("tran_org_id <>", value, "tranOrgId");
            return (Criteria) this;
        }

        public Criteria andTranOrgIdGreaterThan(String value) {
            addCriterion("tran_org_id >", value, "tranOrgId");
            return (Criteria) this;
        }

        public Criteria andTranOrgIdGreaterThanOrEqualTo(String value) {
            addCriterion("tran_org_id >=", value, "tranOrgId");
            return (Criteria) this;
        }

        public Criteria andTranOrgIdLessThan(String value) {
            addCriterion("tran_org_id <", value, "tranOrgId");
            return (Criteria) this;
        }

        public Criteria andTranOrgIdLessThanOrEqualTo(String value) {
            addCriterion("tran_org_id <=", value, "tranOrgId");
            return (Criteria) this;
        }

        public Criteria andTranOrgIdLike(String value) {
            addCriterion("tran_org_id like", value, "tranOrgId");
            return (Criteria) this;
        }

        public Criteria andTranOrgIdNotLike(String value) {
            addCriterion("tran_org_id not like", value, "tranOrgId");
            return (Criteria) this;
        }

        public Criteria andTranOrgIdIn(List<String> values) {
            addCriterion("tran_org_id in", values, "tranOrgId");
            return (Criteria) this;
        }

        public Criteria andTranOrgIdNotIn(List<String> values) {
            addCriterion("tran_org_id not in", values, "tranOrgId");
            return (Criteria) this;
        }

        public Criteria andTranOrgIdBetween(String value1, String value2) {
            addCriterion("tran_org_id between", value1, value2, "tranOrgId");
            return (Criteria) this;
        }

        public Criteria andTranOrgIdNotBetween(String value1, String value2) {
            addCriterion("tran_org_id not between", value1, value2, "tranOrgId");
            return (Criteria) this;
        }

        public Criteria andTranUserIdIsNull() {
            addCriterion("tran_user_id is null");
            return (Criteria) this;
        }

        public Criteria andTranUserIdIsNotNull() {
            addCriterion("tran_user_id is not null");
            return (Criteria) this;
        }

        public Criteria andTranUserIdEqualTo(Long value) {
            addCriterion("tran_user_id =", value, "tranUserId");
            return (Criteria) this;
        }

        public Criteria andTranUserIdNotEqualTo(Long value) {
            addCriterion("tran_user_id <>", value, "tranUserId");
            return (Criteria) this;
        }

        public Criteria andTranUserIdGreaterThan(Long value) {
            addCriterion("tran_user_id >", value, "tranUserId");
            return (Criteria) this;
        }

        public Criteria andTranUserIdGreaterThanOrEqualTo(Long value) {
            addCriterion("tran_user_id >=", value, "tranUserId");
            return (Criteria) this;
        }

        public Criteria andTranUserIdLessThan(Long value) {
            addCriterion("tran_user_id <", value, "tranUserId");
            return (Criteria) this;
        }

        public Criteria andTranUserIdLessThanOrEqualTo(Long value) {
            addCriterion("tran_user_id <=", value, "tranUserId");
            return (Criteria) this;
        }

        public Criteria andTranUserIdIn(List<Long> values) {
            addCriterion("tran_user_id in", values, "tranUserId");
            return (Criteria) this;
        }

        public Criteria andTranUserIdNotIn(List<Long> values) {
            addCriterion("tran_user_id not in", values, "tranUserId");
            return (Criteria) this;
        }

        public Criteria andTranUserIdBetween(Long value1, Long value2) {
            addCriterion("tran_user_id between", value1, value2, "tranUserId");
            return (Criteria) this;
        }

        public Criteria andTranUserIdNotBetween(Long value1, Long value2) {
            addCriterion("tran_user_id not between", value1, value2, "tranUserId");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}