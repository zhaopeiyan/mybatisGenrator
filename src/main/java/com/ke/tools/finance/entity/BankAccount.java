package com.ke.tools.finance.entity;

public class BankAccount {
    private Long id;

    private String orgCode;

    private String bizType;

    private String paidType;

    private String bankType;

    private String accountNo;

    private String bankName;

    private String accountName;

    private String memo;

    private Integer sort;

    private Byte isDelete;

    private String ehomeNo;

    private String ehomeKey;

    private Byte moneyType;

    private Byte available;

    private Byte needCharge;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOrgCode() {
        return orgCode;
    }

    public void setOrgCode(String orgCode) {
        this.orgCode = orgCode;
    }

    public String getBizType() {
        return bizType;
    }

    public void setBizType(String bizType) {
        this.bizType = bizType;
    }

    public String getPaidType() {
        return paidType;
    }

    public void setPaidType(String paidType) {
        this.paidType = paidType;
    }

    public String getBankType() {
        return bankType;
    }

    public void setBankType(String bankType) {
        this.bankType = bankType;
    }

    public String getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public Byte getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Byte isDelete) {
        this.isDelete = isDelete;
    }

    public String getEhomeNo() {
        return ehomeNo;
    }

    public void setEhomeNo(String ehomeNo) {
        this.ehomeNo = ehomeNo;
    }

    public String getEhomeKey() {
        return ehomeKey;
    }

    public void setEhomeKey(String ehomeKey) {
        this.ehomeKey = ehomeKey;
    }

    public Byte getMoneyType() {
        return moneyType;
    }

    public void setMoneyType(Byte moneyType) {
        this.moneyType = moneyType;
    }

    public Byte getAvailable() {
        return available;
    }

    public void setAvailable(Byte available) {
        this.available = available;
    }

    public Byte getNeedCharge() {
        return needCharge;
    }

    public void setNeedCharge(Byte needCharge) {
        this.needCharge = needCharge;
    }
}