package com.ke.tools.finance.invoice.ext.service;

import com.ke.breeze.common.exception.BusinessException;
import com.ke.tools.finance.invoice.ext.dto.BusinessInfoDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import java.util.Map;

@Component
public class ExtBusinessService {

    @Autowired
    private RestTemplate restTemplate;
    @Value("${business.service}")
    private String businessServiceName;

    public BusinessInfoDTO queryBusiness(Map<String,Object> param)throws BusinessException{

        Map<String,Object> resultStr = restTemplate.getForObject("http://"+businessServiceName+"/business?appId={appId}",Map.class,param.get("appId"));
        if (null !=resultStr.get("errorcode") && "0".equals(String.valueOf(resultStr.get("errorcode")))){
           if(resultStr.get("data") !=null){

               Map<String,Object> result = (Map<String, Object>) resultStr.get("data");
               BusinessInfoDTO businessInfoDTO  = new BusinessInfoDTO();

               return  businessInfoDTO.transForm(result);
           }
           return null;
        }else {
            throw new BusinessException(Integer.parseInt(String.valueOf(resultStr.get("errorcode"))),String.valueOf(resultStr.get("errormsg")));
        }
     }


}
