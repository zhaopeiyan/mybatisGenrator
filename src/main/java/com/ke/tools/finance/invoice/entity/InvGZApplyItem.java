package com.ke.tools.finance.invoice.entity;

import java.math.BigDecimal;
import java.util.Date;

public class InvGZApplyItem {
    private Long id;

    private Long invGzApplyId;

    private String itemCode;

    private String itemName;

    private String collectionCompany;

    private BigDecimal amount;

    private Date createdTime;

    private Date updatedTime;

    private Byte available;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getInvGzApplyId() {
        return invGzApplyId;
    }

    public void setInvGzApplyId(Long invGzApplyId) {
        this.invGzApplyId = invGzApplyId;
    }

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getCollectionCompany() {
        return collectionCompany;
    }

    public void setCollectionCompany(String collectionCompany) {
        this.collectionCompany = collectionCompany;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    public Date getUpdatedTime() {
        return updatedTime;
    }

    public void setUpdatedTime(Date updatedTime) {
        this.updatedTime = updatedTime;
    }

    public Byte getAvailable() {
        return available;
    }

    public void setAvailable(Byte available) {
        this.available = available;
    }
}