package com.ke.tools.finance.invoice.entity;

public class InvGZReceiveWay {
    private Long id;

    private Long gzApplyId;

    private Byte type;

    private String content;

    private Byte available;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getGzApplyId() {
        return gzApplyId;
    }

    public void setGzApplyId(Long gzApplyId) {
        this.gzApplyId = gzApplyId;
    }

    public Byte getType() {
        return type;
    }

    public void setType(Byte type) {
        this.type = type;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Byte getAvailable() {
        return available;
    }

    public void setAvailable(Byte available) {
        this.available = available;
    }
}