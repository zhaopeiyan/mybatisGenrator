package com.ke.tools.finance.invoice.entity;

import java.math.BigDecimal;
import java.util.Date;

public class PaidUpItem {
    private Long id;

    private Long contractId;

    private Long paidupId;

    private Long receiptId;

    private String receivableItemCode;

    private String receivableItem;

    private BigDecimal receivableAmount;

    private Long pmId;

    private BigDecimal paidupAmount;

    private String companyCode;

    private String companyName;

    private String memo;

    private Byte status;

    private Date createdTime;

    private String createdBy;

    private Date updatedTime;

    private String updatedBy;

    private Byte edition;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getContractId() {
        return contractId;
    }

    public void setContractId(Long contractId) {
        this.contractId = contractId;
    }

    public Long getPaidupId() {
        return paidupId;
    }

    public void setPaidupId(Long paidupId) {
        this.paidupId = paidupId;
    }

    public Long getReceiptId() {
        return receiptId;
    }

    public void setReceiptId(Long receiptId) {
        this.receiptId = receiptId;
    }

    public String getReceivableItemCode() {
        return receivableItemCode;
    }

    public void setReceivableItemCode(String receivableItemCode) {
        this.receivableItemCode = receivableItemCode;
    }

    public String getReceivableItem() {
        return receivableItem;
    }

    public void setReceivableItem(String receivableItem) {
        this.receivableItem = receivableItem;
    }

    public BigDecimal getReceivableAmount() {
        return receivableAmount;
    }

    public void setReceivableAmount(BigDecimal receivableAmount) {
        this.receivableAmount = receivableAmount;
    }

    public Long getPmId() {
        return pmId;
    }

    public void setPmId(Long pmId) {
        this.pmId = pmId;
    }

    public BigDecimal getPaidupAmount() {
        return paidupAmount;
    }

    public void setPaidupAmount(BigDecimal paidupAmount) {
        this.paidupAmount = paidupAmount;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public Byte getStatus() {
        return status;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getUpdatedTime() {
        return updatedTime;
    }

    public void setUpdatedTime(Date updatedTime) {
        this.updatedTime = updatedTime;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Byte getEdition() {
        return edition;
    }

    public void setEdition(Byte edition) {
        this.edition = edition;
    }
}