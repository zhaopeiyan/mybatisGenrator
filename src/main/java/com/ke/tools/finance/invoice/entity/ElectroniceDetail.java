package com.ke.tools.finance.invoice.entity;

import java.util.Date;

public class ElectroniceDetail {
    private Integer id;

    private Integer infoid;

    private String xmmcfield;

    private String dwfield;

    private String ggxhfield;

    private Double xmslfield;

    private String hsbzfield;

    private Double xmdjfield;

    private String xmbmfield;

    private String spbmfield;

    private String zxbmfield;

    private String yhzcbsfield;

    private String lslbsfield;

    private String zzstsglfield;

    private Double xmjefield;

    private String slfield;

    private Double sefield;

    private Double kcefield;

    private Date createtime;

    private String fps3url;

    private String dowurl;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getInfoid() {
        return infoid;
    }

    public void setInfoid(Integer infoid) {
        this.infoid = infoid;
    }

    public String getXmmcfield() {
        return xmmcfield;
    }

    public void setXmmcfield(String xmmcfield) {
        this.xmmcfield = xmmcfield;
    }

    public String getDwfield() {
        return dwfield;
    }

    public void setDwfield(String dwfield) {
        this.dwfield = dwfield;
    }

    public String getGgxhfield() {
        return ggxhfield;
    }

    public void setGgxhfield(String ggxhfield) {
        this.ggxhfield = ggxhfield;
    }

    public Double getXmslfield() {
        return xmslfield;
    }

    public void setXmslfield(Double xmslfield) {
        this.xmslfield = xmslfield;
    }

    public String getHsbzfield() {
        return hsbzfield;
    }

    public void setHsbzfield(String hsbzfield) {
        this.hsbzfield = hsbzfield;
    }

    public Double getXmdjfield() {
        return xmdjfield;
    }

    public void setXmdjfield(Double xmdjfield) {
        this.xmdjfield = xmdjfield;
    }

    public String getXmbmfield() {
        return xmbmfield;
    }

    public void setXmbmfield(String xmbmfield) {
        this.xmbmfield = xmbmfield;
    }

    public String getSpbmfield() {
        return spbmfield;
    }

    public void setSpbmfield(String spbmfield) {
        this.spbmfield = spbmfield;
    }

    public String getZxbmfield() {
        return zxbmfield;
    }

    public void setZxbmfield(String zxbmfield) {
        this.zxbmfield = zxbmfield;
    }

    public String getYhzcbsfield() {
        return yhzcbsfield;
    }

    public void setYhzcbsfield(String yhzcbsfield) {
        this.yhzcbsfield = yhzcbsfield;
    }

    public String getLslbsfield() {
        return lslbsfield;
    }

    public void setLslbsfield(String lslbsfield) {
        this.lslbsfield = lslbsfield;
    }

    public String getZzstsglfield() {
        return zzstsglfield;
    }

    public void setZzstsglfield(String zzstsglfield) {
        this.zzstsglfield = zzstsglfield;
    }

    public Double getXmjefield() {
        return xmjefield;
    }

    public void setXmjefield(Double xmjefield) {
        this.xmjefield = xmjefield;
    }

    public String getSlfield() {
        return slfield;
    }

    public void setSlfield(String slfield) {
        this.slfield = slfield;
    }

    public Double getSefield() {
        return sefield;
    }

    public void setSefield(Double sefield) {
        this.sefield = sefield;
    }

    public Double getKcefield() {
        return kcefield;
    }

    public void setKcefield(Double kcefield) {
        this.kcefield = kcefield;
    }

    public Date getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }

    public String getFps3url() {
        return fps3url;
    }

    public void setFps3url(String fps3url) {
        this.fps3url = fps3url;
    }

    public String getDowurl() {
        return dowurl;
    }

    public void setDowurl(String dowurl) {
        this.dowurl = dowurl;
    }
}