package com.ke.tools.finance.invoice.entity;

import java.util.Date;

public class ElectroniceInfo {
    private Integer id;

    private Long paidupid;

    private String fpqqlshfield;

    private String bmbBbhfield;

    private String czdmfield;

    private String ddhfield;

    private String dkbzfield;

    private String ghfMcfield;

    private String xhfYhzhfield;

    private String xhfMcfield;

    private String xhfNsrsbhfield;

    private String xhfDhfield;

    private String xhfDzfield;

    private Double kphjjefield;

    private String kplxfield;

    private String kpxmfield;

    private String kpNsrmcfield;

    private String kpNsrsbhfield;

    private String kprfield;

    private String bzfield;

    private String chyyfield;

    private String dsptbmfield;

    private String emailfield;

    private String ewmfield;

    private String fhrfield;

    private String fpztfield;

    private String fpDmfield;

    private String fpHmfield;

    private String fpMwfield;

    private String ghfqylxfield;

    private String ghfDzfield;

    private String ghfEmailfield;

    private String ghfGddhfield;

    private String ghfNsrsbhfield;

    private String ghfSffield;

    private String ghfSjfield;

    private String ghfYhzhfield;

    private Double hjbhsjefield;

    private Byte hjbhsjefieldspecified;

    private Double hjsefield;

    private Byte hjsefieldspecified;

    private String hyDmfield;

    private String hyMcfield;

    private String jqbhfield;

    private String jymfield;

    private Byte kphjjefieldspecified;

    private String kprqfield;

    private String nsrdzdahfield;

    private String pydmfield;

    private String sjfield;

    private String skrfield;

    private String swjgDmfield;

    private String tschbzfield;

    private String tsfsfield;

    private String yfpDmfield;

    private String yfpHmfield;

    private String appidfield;

    private String authorizationcodefield;

    private String codetypefield;

    private String contentfield;

    private String dataexchangeidfield;

    private String encryptcodefield;

    private String interfacecodefield;

    private String passwordfield;

    private String requestcodefield;

    private String responsecodefield;

    private String terminalcodefield;

    private String usernamefield;

    private String versionfield;

    private Date createtime;

    private Long invoicestatus;

    private Date updatetime;

    private String xfpDmfield;

    private String xfpHmfield;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Long getPaidupid() {
        return paidupid;
    }

    public void setPaidupid(Long paidupid) {
        this.paidupid = paidupid;
    }

    public String getFpqqlshfield() {
        return fpqqlshfield;
    }

    public void setFpqqlshfield(String fpqqlshfield) {
        this.fpqqlshfield = fpqqlshfield;
    }

    public String getBmbBbhfield() {
        return bmbBbhfield;
    }

    public void setBmbBbhfield(String bmbBbhfield) {
        this.bmbBbhfield = bmbBbhfield;
    }

    public String getCzdmfield() {
        return czdmfield;
    }

    public void setCzdmfield(String czdmfield) {
        this.czdmfield = czdmfield;
    }

    public String getDdhfield() {
        return ddhfield;
    }

    public void setDdhfield(String ddhfield) {
        this.ddhfield = ddhfield;
    }

    public String getDkbzfield() {
        return dkbzfield;
    }

    public void setDkbzfield(String dkbzfield) {
        this.dkbzfield = dkbzfield;
    }

    public String getGhfMcfield() {
        return ghfMcfield;
    }

    public void setGhfMcfield(String ghfMcfield) {
        this.ghfMcfield = ghfMcfield;
    }

    public String getXhfYhzhfield() {
        return xhfYhzhfield;
    }

    public void setXhfYhzhfield(String xhfYhzhfield) {
        this.xhfYhzhfield = xhfYhzhfield;
    }

    public String getXhfMcfield() {
        return xhfMcfield;
    }

    public void setXhfMcfield(String xhfMcfield) {
        this.xhfMcfield = xhfMcfield;
    }

    public String getXhfNsrsbhfield() {
        return xhfNsrsbhfield;
    }

    public void setXhfNsrsbhfield(String xhfNsrsbhfield) {
        this.xhfNsrsbhfield = xhfNsrsbhfield;
    }

    public String getXhfDhfield() {
        return xhfDhfield;
    }

    public void setXhfDhfield(String xhfDhfield) {
        this.xhfDhfield = xhfDhfield;
    }

    public String getXhfDzfield() {
        return xhfDzfield;
    }

    public void setXhfDzfield(String xhfDzfield) {
        this.xhfDzfield = xhfDzfield;
    }

    public Double getKphjjefield() {
        return kphjjefield;
    }

    public void setKphjjefield(Double kphjjefield) {
        this.kphjjefield = kphjjefield;
    }

    public String getKplxfield() {
        return kplxfield;
    }

    public void setKplxfield(String kplxfield) {
        this.kplxfield = kplxfield;
    }

    public String getKpxmfield() {
        return kpxmfield;
    }

    public void setKpxmfield(String kpxmfield) {
        this.kpxmfield = kpxmfield;
    }

    public String getKpNsrmcfield() {
        return kpNsrmcfield;
    }

    public void setKpNsrmcfield(String kpNsrmcfield) {
        this.kpNsrmcfield = kpNsrmcfield;
    }

    public String getKpNsrsbhfield() {
        return kpNsrsbhfield;
    }

    public void setKpNsrsbhfield(String kpNsrsbhfield) {
        this.kpNsrsbhfield = kpNsrsbhfield;
    }

    public String getKprfield() {
        return kprfield;
    }

    public void setKprfield(String kprfield) {
        this.kprfield = kprfield;
    }

    public String getBzfield() {
        return bzfield;
    }

    public void setBzfield(String bzfield) {
        this.bzfield = bzfield;
    }

    public String getChyyfield() {
        return chyyfield;
    }

    public void setChyyfield(String chyyfield) {
        this.chyyfield = chyyfield;
    }

    public String getDsptbmfield() {
        return dsptbmfield;
    }

    public void setDsptbmfield(String dsptbmfield) {
        this.dsptbmfield = dsptbmfield;
    }

    public String getEmailfield() {
        return emailfield;
    }

    public void setEmailfield(String emailfield) {
        this.emailfield = emailfield;
    }

    public String getEwmfield() {
        return ewmfield;
    }

    public void setEwmfield(String ewmfield) {
        this.ewmfield = ewmfield;
    }

    public String getFhrfield() {
        return fhrfield;
    }

    public void setFhrfield(String fhrfield) {
        this.fhrfield = fhrfield;
    }

    public String getFpztfield() {
        return fpztfield;
    }

    public void setFpztfield(String fpztfield) {
        this.fpztfield = fpztfield;
    }

    public String getFpDmfield() {
        return fpDmfield;
    }

    public void setFpDmfield(String fpDmfield) {
        this.fpDmfield = fpDmfield;
    }

    public String getFpHmfield() {
        return fpHmfield;
    }

    public void setFpHmfield(String fpHmfield) {
        this.fpHmfield = fpHmfield;
    }

    public String getFpMwfield() {
        return fpMwfield;
    }

    public void setFpMwfield(String fpMwfield) {
        this.fpMwfield = fpMwfield;
    }

    public String getGhfqylxfield() {
        return ghfqylxfield;
    }

    public void setGhfqylxfield(String ghfqylxfield) {
        this.ghfqylxfield = ghfqylxfield;
    }

    public String getGhfDzfield() {
        return ghfDzfield;
    }

    public void setGhfDzfield(String ghfDzfield) {
        this.ghfDzfield = ghfDzfield;
    }

    public String getGhfEmailfield() {
        return ghfEmailfield;
    }

    public void setGhfEmailfield(String ghfEmailfield) {
        this.ghfEmailfield = ghfEmailfield;
    }

    public String getGhfGddhfield() {
        return ghfGddhfield;
    }

    public void setGhfGddhfield(String ghfGddhfield) {
        this.ghfGddhfield = ghfGddhfield;
    }

    public String getGhfNsrsbhfield() {
        return ghfNsrsbhfield;
    }

    public void setGhfNsrsbhfield(String ghfNsrsbhfield) {
        this.ghfNsrsbhfield = ghfNsrsbhfield;
    }

    public String getGhfSffield() {
        return ghfSffield;
    }

    public void setGhfSffield(String ghfSffield) {
        this.ghfSffield = ghfSffield;
    }

    public String getGhfSjfield() {
        return ghfSjfield;
    }

    public void setGhfSjfield(String ghfSjfield) {
        this.ghfSjfield = ghfSjfield;
    }

    public String getGhfYhzhfield() {
        return ghfYhzhfield;
    }

    public void setGhfYhzhfield(String ghfYhzhfield) {
        this.ghfYhzhfield = ghfYhzhfield;
    }

    public Double getHjbhsjefield() {
        return hjbhsjefield;
    }

    public void setHjbhsjefield(Double hjbhsjefield) {
        this.hjbhsjefield = hjbhsjefield;
    }

    public Byte getHjbhsjefieldspecified() {
        return hjbhsjefieldspecified;
    }

    public void setHjbhsjefieldspecified(Byte hjbhsjefieldspecified) {
        this.hjbhsjefieldspecified = hjbhsjefieldspecified;
    }

    public Double getHjsefield() {
        return hjsefield;
    }

    public void setHjsefield(Double hjsefield) {
        this.hjsefield = hjsefield;
    }

    public Byte getHjsefieldspecified() {
        return hjsefieldspecified;
    }

    public void setHjsefieldspecified(Byte hjsefieldspecified) {
        this.hjsefieldspecified = hjsefieldspecified;
    }

    public String getHyDmfield() {
        return hyDmfield;
    }

    public void setHyDmfield(String hyDmfield) {
        this.hyDmfield = hyDmfield;
    }

    public String getHyMcfield() {
        return hyMcfield;
    }

    public void setHyMcfield(String hyMcfield) {
        this.hyMcfield = hyMcfield;
    }

    public String getJqbhfield() {
        return jqbhfield;
    }

    public void setJqbhfield(String jqbhfield) {
        this.jqbhfield = jqbhfield;
    }

    public String getJymfield() {
        return jymfield;
    }

    public void setJymfield(String jymfield) {
        this.jymfield = jymfield;
    }

    public Byte getKphjjefieldspecified() {
        return kphjjefieldspecified;
    }

    public void setKphjjefieldspecified(Byte kphjjefieldspecified) {
        this.kphjjefieldspecified = kphjjefieldspecified;
    }

    public String getKprqfield() {
        return kprqfield;
    }

    public void setKprqfield(String kprqfield) {
        this.kprqfield = kprqfield;
    }

    public String getNsrdzdahfield() {
        return nsrdzdahfield;
    }

    public void setNsrdzdahfield(String nsrdzdahfield) {
        this.nsrdzdahfield = nsrdzdahfield;
    }

    public String getPydmfield() {
        return pydmfield;
    }

    public void setPydmfield(String pydmfield) {
        this.pydmfield = pydmfield;
    }

    public String getSjfield() {
        return sjfield;
    }

    public void setSjfield(String sjfield) {
        this.sjfield = sjfield;
    }

    public String getSkrfield() {
        return skrfield;
    }

    public void setSkrfield(String skrfield) {
        this.skrfield = skrfield;
    }

    public String getSwjgDmfield() {
        return swjgDmfield;
    }

    public void setSwjgDmfield(String swjgDmfield) {
        this.swjgDmfield = swjgDmfield;
    }

    public String getTschbzfield() {
        return tschbzfield;
    }

    public void setTschbzfield(String tschbzfield) {
        this.tschbzfield = tschbzfield;
    }

    public String getTsfsfield() {
        return tsfsfield;
    }

    public void setTsfsfield(String tsfsfield) {
        this.tsfsfield = tsfsfield;
    }

    public String getYfpDmfield() {
        return yfpDmfield;
    }

    public void setYfpDmfield(String yfpDmfield) {
        this.yfpDmfield = yfpDmfield;
    }

    public String getYfpHmfield() {
        return yfpHmfield;
    }

    public void setYfpHmfield(String yfpHmfield) {
        this.yfpHmfield = yfpHmfield;
    }

    public String getAppidfield() {
        return appidfield;
    }

    public void setAppidfield(String appidfield) {
        this.appidfield = appidfield;
    }

    public String getAuthorizationcodefield() {
        return authorizationcodefield;
    }

    public void setAuthorizationcodefield(String authorizationcodefield) {
        this.authorizationcodefield = authorizationcodefield;
    }

    public String getCodetypefield() {
        return codetypefield;
    }

    public void setCodetypefield(String codetypefield) {
        this.codetypefield = codetypefield;
    }

    public String getContentfield() {
        return contentfield;
    }

    public void setContentfield(String contentfield) {
        this.contentfield = contentfield;
    }

    public String getDataexchangeidfield() {
        return dataexchangeidfield;
    }

    public void setDataexchangeidfield(String dataexchangeidfield) {
        this.dataexchangeidfield = dataexchangeidfield;
    }

    public String getEncryptcodefield() {
        return encryptcodefield;
    }

    public void setEncryptcodefield(String encryptcodefield) {
        this.encryptcodefield = encryptcodefield;
    }

    public String getInterfacecodefield() {
        return interfacecodefield;
    }

    public void setInterfacecodefield(String interfacecodefield) {
        this.interfacecodefield = interfacecodefield;
    }

    public String getPasswordfield() {
        return passwordfield;
    }

    public void setPasswordfield(String passwordfield) {
        this.passwordfield = passwordfield;
    }

    public String getRequestcodefield() {
        return requestcodefield;
    }

    public void setRequestcodefield(String requestcodefield) {
        this.requestcodefield = requestcodefield;
    }

    public String getResponsecodefield() {
        return responsecodefield;
    }

    public void setResponsecodefield(String responsecodefield) {
        this.responsecodefield = responsecodefield;
    }

    public String getTerminalcodefield() {
        return terminalcodefield;
    }

    public void setTerminalcodefield(String terminalcodefield) {
        this.terminalcodefield = terminalcodefield;
    }

    public String getUsernamefield() {
        return usernamefield;
    }

    public void setUsernamefield(String usernamefield) {
        this.usernamefield = usernamefield;
    }

    public String getVersionfield() {
        return versionfield;
    }

    public void setVersionfield(String versionfield) {
        this.versionfield = versionfield;
    }

    public Date getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }

    public Long getInvoicestatus() {
        return invoicestatus;
    }

    public void setInvoicestatus(Long invoicestatus) {
        this.invoicestatus = invoicestatus;
    }

    public Date getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(Date updatetime) {
        this.updatetime = updatetime;
    }

    public String getXfpDmfield() {
        return xfpDmfield;
    }

    public void setXfpDmfield(String xfpDmfield) {
        this.xfpDmfield = xfpDmfield;
    }

    public String getXfpHmfield() {
        return xfpHmfield;
    }

    public void setXfpHmfield(String xfpHmfield) {
        this.xfpHmfield = xfpHmfield;
    }
}