package com.ke.tools.finance.invoice.ext.dto;
import java.util.Map;

/**
 * @author zhaoyunping
 * @Title: BusinessInfoDto
 * @ProjectName finance-invoice
 * @Description: 外部接入方服务实体
 * @date 2018/8/316:57
 */
public class BusinessInfoDTO {

    private long businessId;

    private String businessName;

    private String appId;

    private String appSecret;

    private int status;

    private String createDate;

    private String url;

    public long getBusinessId() {
        return businessId;
    }

    public void setBusinessId(long businessId) {
        this.businessId = businessId;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getAppSecret() {
        return appSecret;
    }

    public void setAppSecret(String appSecret) {
        this.appSecret = appSecret;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
    public BusinessInfoDTO transForm(Map<String,Object> param){

        if( null != param ){
            if(null != param.get("id")){
                this.setBusinessId(Long.parseLong(String.valueOf(param.get("id"))));
            }
            if(null != param.get("businessName")){
                this.setBusinessName(String.valueOf(param.get("businessName")));
            }
            if(null != param.get("appId")){
                this.setAppId(String.valueOf(param.get("appId")));
            }
            if(null != param.get("appSecret")){
                this.setAppSecret(String.valueOf(param.get("appSecret")));
            }
            if(null != param.get("status")){
                this.setStatus(Integer.parseInt(String.valueOf(param.get("status"))));
            }
            if(null != param.get("createDate")){
                this.setCreateDate(String.valueOf(param.get("createDate")));
            }
            if(null != param.get("url")){
                this.setUrl(String.valueOf(param.get("url")));
            }
        }
        return  this;

    }

}
