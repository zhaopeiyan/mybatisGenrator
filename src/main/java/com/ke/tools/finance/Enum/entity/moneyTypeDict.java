package com.ke.tools.finance.Enum.entity;

import java.util.Date;

public class moneyTypeDict {
    private Integer id;

    private String moneytypeCode;

    private String moneytypeValue;

    private String moneytypeName;

    private String payertypeCode;

    private Date createDate;

    private Boolean isvalid;

    private String desc;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMoneytypeCode() {
        return moneytypeCode;
    }

    public void setMoneytypeCode(String moneytypeCode) {
        this.moneytypeCode = moneytypeCode;
    }

    public String getMoneytypeValue() {
        return moneytypeValue;
    }

    public void setMoneytypeValue(String moneytypeValue) {
        this.moneytypeValue = moneytypeValue;
    }

    public String getMoneytypeName() {
        return moneytypeName;
    }

    public void setMoneytypeName(String moneytypeName) {
        this.moneytypeName = moneytypeName;
    }

    public String getPayertypeCode() {
        return payertypeCode;
    }

    public void setPayertypeCode(String payertypeCode) {
        this.payertypeCode = payertypeCode;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Boolean getIsvalid() {
        return isvalid;
    }

    public void setIsvalid(Boolean isvalid) {
        this.isvalid = isvalid;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}