package com.ke.tools.finance.Enum;

/**
 * Created by huranran on 2018/1/9.
 */
public enum RoleType {
	COMPLETE(0, "不分角色"),
	HOUSE(1, "房源角色"),
	NOT_HOUSE(2, "非房角色");

	private Integer code;
	private String desc;

	RoleType(Integer code, String desc) {
		this.code = code;
		this.desc = desc;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}
}
