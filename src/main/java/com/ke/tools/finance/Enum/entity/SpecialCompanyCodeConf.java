package com.ke.tools.finance.Enum.entity;

import java.util.Date;

public class SpecialCompanyCodeConf {
    private Long id;

    private String companyCode;

    private String specialCompanyCode;

    private Byte status;

    private Date createdTime;

    private String createdBy;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public String getSpecialCompanyCode() {
        return specialCompanyCode;
    }

    public void setSpecialCompanyCode(String specialCompanyCode) {
        this.specialCompanyCode = specialCompanyCode;
    }

    public Byte getStatus() {
        return status;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }
}