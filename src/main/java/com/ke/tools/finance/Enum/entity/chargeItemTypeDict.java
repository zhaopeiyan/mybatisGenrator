package com.ke.tools.finance.Enum.entity;

import java.util.Date;

public class chargeItemTypeDict {
    private Integer id;

    private String chargeitemCode;

    private String chargeitemValue;

    private String chargeitemName;

    private String moneytypeCode;

    private Date createDate;

    private Boolean isvalid;

    private String desc;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getChargeitemCode() {
        return chargeitemCode;
    }

    public void setChargeitemCode(String chargeitemCode) {
        this.chargeitemCode = chargeitemCode;
    }

    public String getChargeitemValue() {
        return chargeitemValue;
    }

    public void setChargeitemValue(String chargeitemValue) {
        this.chargeitemValue = chargeitemValue;
    }

    public String getChargeitemName() {
        return chargeitemName;
    }

    public void setChargeitemName(String chargeitemName) {
        this.chargeitemName = chargeitemName;
    }

    public String getMoneytypeCode() {
        return moneytypeCode;
    }

    public void setMoneytypeCode(String moneytypeCode) {
        this.moneytypeCode = moneytypeCode;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Boolean getIsvalid() {
        return isvalid;
    }

    public void setIsvalid(Boolean isvalid) {
        this.isvalid = isvalid;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}