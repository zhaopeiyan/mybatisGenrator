package com.ke.tools.finance.Enum;


/**
 * Created by binbin on 16/5/30.
 */
public enum ChargeItemTypeEnum {

	/**
	 * 未知 0
	 */
	UnKnown("-2", "未知", MoneyTypeEnum.YONGJIN),
	OTHER("-1", "多收款需退费", MoneyTypeEnum.YONGJIN),
//    OTHER("-1", "其他"),

	AGENCY_FEE("1", "居间代理费", MoneyTypeEnum.YONGJIN),
	PING_GU_FEE("2", "代收评估费", MoneyTypeEnum.YONGJIN),
	//    PING_GU_FEE("2","评估费"),
	JIN_RONG_FUWU_FEE("3", "贷款服务费", MoneyTypeEnum.YONGJIN),
	//    JIN_RONG_FUWU_FEE("3","金融服务费"),
	GUO_HU_FUWU_FEE("4", "过户服务费", MoneyTypeEnum.YONGJIN),
	BAO_ZHANG_FUWU_FEE("5", "保障服务费", MoneyTypeEnum.YONGJIN),
	//   AN_JIE_FUWU_FEE("5","贷款服务费"),
//    AN_JIE_FUWU_FEE("5","按揭服务费"),
	KEHU_CHENGYI_JIN("6", "客户诚意金", MoneyTypeEnum.YONGJIN),
	DAIBAN_FUwu_FEE("7", "代办服务费", MoneyTypeEnum.YONGJIN),
	QUANZHENG_DAIBAN_FEE("8", "权证代办费", MoneyTypeEnum.YONGJIN),

	DAISHOU_YIXIANGJIN_MM_FEE("9", "代收意向金", MoneyTypeEnum.YIXIANGJIN),
	DAISHOU_DINDJIN_MM_FEE("10", "代收定金", MoneyTypeEnum.DINGJIN),
	DAISHOU_JIAOFANGBAOZHENGJIN_FEE("11", "代收交房保证金", MoneyTypeEnum.JFBAOZHENGJIN),
	DAISHOU_YIXIANGJIN_ZL_FEE("12", "租赁意向金", MoneyTypeEnum.YIXIANGJIN),
	DAISHOU_DINDJIN_ZL_FEE("13", "租赁定金", MoneyTypeEnum.DINGJIN),
	ANJIE_GUO_HU_FUWU_FEE("14", "按揭过户服务费", MoneyTypeEnum.YONGJIN),
	DAISHOU_WUYEBAOZHENGJIN_MM_FEE("15", "代收物业保证金", MoneyTypeEnum.WUYEBAOZHENGJIN),
	JUJIAN_FUWU_FEE("17", "居间服务费", MoneyTypeEnum.YONGJIN),
	QUANZHENG_FUWU_FEE("18", "权证服务费", MoneyTypeEnum.YONGJIN),
	JIAOYI_FUWU_FEE("19", "交易服务费", MoneyTypeEnum.YIXIANGJIN),
	ANJIE_FUWU_FEE("22", "按揭服务费", MoneyTypeEnum.YONGJIN),
	ZHONFJIE_FEE("23", "中介费", MoneyTypeEnum.YONGJIN),
	DAIBAN_FEE("24", "代办费", MoneyTypeEnum.YONGJIN),
	BANJIAN_FEE("25", "办件费", MoneyTypeEnum.YONGJIN),
	TENGFANG_YAJIN_FEE("163", "腾房押金", MoneyTypeEnum.TENGFANGYAJIN),
	DAISHOU_HUKOUQIANCHU_FEE("117", "代收户口迁出保证金", MoneyTypeEnum.HUKOUQIANCHUBAOZHENGJIN),
	DAISHOU_JIAOFANGHEHUKOUQIANCHU_FEE("118", "代收交房和户口迁出保证金", MoneyTypeEnum.JIAOFANGHEHUKOUQIANCHUBAOZHENGJIN),
	DAIKUAN_DAIBAN_FEE("26", "贷款代办费", MoneyTypeEnum.YONGJIN),
	PINFGU_FEE("27", "评估费", MoneyTypeEnum.YONGJIN),
	ANJIE_SHOUXU_FEE("28", "按揭手续费", MoneyTypeEnum.YONGJIN),
	QUANZHENG_SHOUXU_FEE("29", "权证手续费", MoneyTypeEnum.YONGJIN),
	GUOHU_FEE("30", "过户费", MoneyTypeEnum.YONGJIN),
	DAISHOU_JIAOLOUBAOZHENGJIN_FEE("239", "代收交楼保证金", MoneyTypeEnum.JIAOLOUBAOZHENGJIN),
	DAISHOU_HUJIBAOZHENGJIN_FEE("240", "代收户籍保证金", MoneyTypeEnum.HUJIBAOZHENGJIN),
	ZULIN_YAJIN_FEE("241", "租赁押金", MoneyTypeEnum.ZULINYAJIN),
	JIAOYIBAOZHANGFUWUFEI("31", "交易保障服务费", MoneyTypeEnum.YONGJIN),
	FUWU_FEE("32", "服务费", MoneyTypeEnum.YONGJIN),
	DAIBANANJIEDAIKUAN_FUWU_FEE("33", "代办按揭贷款服务费", MoneyTypeEnum.YONGJIN),
	YUDAISHOU_PINGGU_FEE("34", "预代收评估费", MoneyTypeEnum.YONGJIN),;
	// DAI_KUAN_FUWU_FEE("8","贷款服务费");

	private String code;

	private String text;

	private MoneyTypeEnum moneyTypeEnum;

	ChargeItemTypeEnum(String code, String text, MoneyTypeEnum moneyTypeEnum) {
		this.code = code;
		this.text = text;
		this.moneyTypeEnum = moneyTypeEnum;
	}

	public String getText() {
		return text;
	}

	public String getCode() {
		return code;
	}

	/*========================================根据code获取相应枚举项========================================*/

	/**
	 * 根据code获得对应的枚举项
	 *
	 * @param code
	 * @return
	 */
	public static ChargeItemTypeEnum get(String code) {
		if (code == null) return null;
		for (ChargeItemTypeEnum cur : ChargeItemTypeEnum.values()) {
			if (code.equals(cur.getCode())) {
				return cur;
			}
		}
		return null;
	}

	/**
	 * 根据code获得对应的枚举项的文本信息
	 *
	 * @param code
	 * @return
	 */
	public static String getEnumText(String code) {
		if (code == null) return ChargeItemTypeEnum.UnKnown.text;
		for (ChargeItemTypeEnum cur : ChargeItemTypeEnum.values()) {
			if (cur.getCode().equals(code)) {
				return cur.text;
			}
		}
		return ChargeItemTypeEnum.UnKnown.text;
	}

	public static String getEnumCode(String text) {
		if (text == null) return ChargeItemTypeEnum.UnKnown.code;
		for (ChargeItemTypeEnum cur : ChargeItemTypeEnum.values()) {
			if (cur.getText().equals(text)) {
				return cur.code;
			}
		}
		return ChargeItemTypeEnum.UnKnown.code;
	}

}
