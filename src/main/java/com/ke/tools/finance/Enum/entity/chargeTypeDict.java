package com.ke.tools.finance.Enum.entity;

import java.util.Date;

public class chargeTypeDict {
    private Integer id;

    private String chargetypeCode;

    private String chargetypeValue;

    private String chargetypeName;

    private String datatypeCode;

    private Date createDate;

    private Boolean isvalid;

    private String desc;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getChargetypeCode() {
        return chargetypeCode;
    }

    public void setChargetypeCode(String chargetypeCode) {
        this.chargetypeCode = chargetypeCode;
    }

    public String getChargetypeValue() {
        return chargetypeValue;
    }

    public void setChargetypeValue(String chargetypeValue) {
        this.chargetypeValue = chargetypeValue;
    }

    public String getChargetypeName() {
        return chargetypeName;
    }

    public void setChargetypeName(String chargetypeName) {
        this.chargetypeName = chargetypeName;
    }

    public String getDatatypeCode() {
        return datatypeCode;
    }

    public void setDatatypeCode(String datatypeCode) {
        this.datatypeCode = datatypeCode;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Boolean getIsvalid() {
        return isvalid;
    }

    public void setIsvalid(Boolean isvalid) {
        this.isvalid = isvalid;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}