package com.ke.tools.finance.Enum.entity;

import java.util.Date;

public class CompanyConf {
    private Long id;

    private String cityCode;

    private String companyCode;

    private String companyName;

    private String coCompanyCode;

    private String coCompanyName;

    private String receivableItemCode;

    private String receivableItem;

    private String bizType;

    private String chargeCode;

    private String chargeName;

    private Byte divideLevel;

    private String divideCfg;

    private Byte status;

    private Byte version;

    private Date effectiveTime;

    private Date createdTime;

    private Date updateTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCityCode() {
        return cityCode;
    }

    public void setCityCode(String cityCode) {
        this.cityCode = cityCode;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCoCompanyCode() {
        return coCompanyCode;
    }

    public void setCoCompanyCode(String coCompanyCode) {
        this.coCompanyCode = coCompanyCode;
    }

    public String getCoCompanyName() {
        return coCompanyName;
    }

    public void setCoCompanyName(String coCompanyName) {
        this.coCompanyName = coCompanyName;
    }

    public String getReceivableItemCode() {
        return receivableItemCode;
    }

    public void setReceivableItemCode(String receivableItemCode) {
        this.receivableItemCode = receivableItemCode;
    }

    public String getReceivableItem() {
        return receivableItem;
    }

    public void setReceivableItem(String receivableItem) {
        this.receivableItem = receivableItem;
    }

    public String getBizType() {
        return bizType;
    }

    public void setBizType(String bizType) {
        this.bizType = bizType;
    }

    public String getChargeCode() {
        return chargeCode;
    }

    public void setChargeCode(String chargeCode) {
        this.chargeCode = chargeCode;
    }

    public String getChargeName() {
        return chargeName;
    }

    public void setChargeName(String chargeName) {
        this.chargeName = chargeName;
    }

    public Byte getDivideLevel() {
        return divideLevel;
    }

    public void setDivideLevel(Byte divideLevel) {
        this.divideLevel = divideLevel;
    }

    public String getDivideCfg() {
        return divideCfg;
    }

    public void setDivideCfg(String divideCfg) {
        this.divideCfg = divideCfg;
    }

    public Byte getStatus() {
        return status;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }

    public Byte getVersion() {
        return version;
    }

    public void setVersion(Byte version) {
        this.version = version;
    }

    public Date getEffectiveTime() {
        return effectiveTime;
    }

    public void setEffectiveTime(Date effectiveTime) {
        this.effectiveTime = effectiveTime;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}