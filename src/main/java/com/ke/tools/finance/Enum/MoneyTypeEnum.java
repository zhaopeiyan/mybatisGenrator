package com.ke.tools.finance.Enum;


/**
 * Created by dongmingbin on 16/12/7.
 */
public enum MoneyTypeEnum {

    //若增加资金类型，需要在ReceivableBizImpl类中的genOrder方法增加一种资金类型
    YIXIANGJIN(1,"意向金", PayerTypeEnum.PAYER_BUY),
    DINGJIN(2,"定金", PayerTypeEnum.PAYER_SELL),
    JFBAOZHENGJIN(3,"交房保证金", PayerTypeEnum.PAYER_SELL),
    YONGJIN(4,"代理费", null),
    WUYEBAOZHENGJIN(5, "物业保证金", PayerTypeEnum.PAYER_SELL),
    HUKOUQIANCHUBAOZHENGJIN(6, "户口迁出保证金", PayerTypeEnum.PAYER_SELL),
    JIAOFANGHEHUKOUQIANCHUBAOZHENGJIN(7, "交房和户口迁出保证金", PayerTypeEnum.PAYER_SELL),
    TENGFANGYAJIN(8, "腾房押金", PayerTypeEnum.PAYER_SELL),
    JIAOLOUBAOZHENGJIN(9, "交楼保证金", PayerTypeEnum.PAYER_SELL),
    HUJIBAOZHENGJIN(10, "户籍保证金", PayerTypeEnum.PAYER_SELL),
    ZULINYAJIN(11, "租赁押金", PayerTypeEnum.PAYER_BUY),
    ;
    Integer code;
    String desc;
    PayerTypeEnum payerTypeEnum;  //该资金类型收款时的客服类型，为null时说明 客户、业主都可以收

    MoneyTypeEnum(Integer code, String desc, PayerTypeEnum payerTypeEnum){
        this.code=code;
        this.desc=desc;
        this.payerTypeEnum = payerTypeEnum;
    }

    public Integer getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }


    public PayerTypeEnum getPayerTypeEnum() {
        return payerTypeEnum;
    }

    public void setPayerTypeEnum(PayerTypeEnum payerTypeEnum) {
        this.payerTypeEnum = payerTypeEnum;
    }


    public static MoneyTypeEnum getEnumByDesc(String desc){
        if(desc == null){
            return null;
        }
        for(MoneyTypeEnum moneyTypeEnum : MoneyTypeEnum.values()){
            if(moneyTypeEnum.getDesc().equals(desc)){
                return moneyTypeEnum;
            }
        }
        return null;
    }

    //客户和业务要共享时，客户业主共享计算金额
    public static Boolean sharedMoney(Integer code){
        if(code == null){
            return false;
        }
        if(MoneyTypeEnum.YONGJIN.getCode().equals(code)){
            return false;
        }
        return true;
    }
}
