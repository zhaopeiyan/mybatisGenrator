package com.ke.tools.finance.Enum;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by binbin on 15/5/26.
 */
public enum PayerTypeEnum {

	/**
	 * 实收财务对账成功
	 */
	PAYER_BUY("1", "客户"),

	/**
	 * 实收财务未对账
	 */
	PAYER_SELL("2", "业主");

	private String code;

	private String text;

	private static Map<String, String> payerCodeNameMap = new HashMap<>();

	static {
		for (PayerTypeEnum typeEnum : PayerTypeEnum.values()) {
			payerCodeNameMap.put(typeEnum.getCode(), typeEnum.getText());
		}
	}

	/**
	 * 通过code 获取 名称
	 *
	 * @param code
	 * @return
	 */
	public static String getNameByCode(String code) {
		return payerCodeNameMap.get(code);
	}


	PayerTypeEnum(String code, String text) {
		this.code = code;
		this.text = text;
	}

	public String getText() {
		return text;
	}

	public String getCode() {
		return code;
	}

	/*========================================根据code获取相应枚举项========================================*/

	/**
	 * 根据code获得对应的枚举项
	 *
	 * @param code
	 * @return
	 */
	public static PayerTypeEnum get(String code) {
		if (code == null) return null;
		for (PayerTypeEnum cur : PayerTypeEnum.values()) {
			if (code.equals(cur.getCode())) {
				return cur;
			}
		}
		return null;
	}

	/**
	 * 根据code获得对应的枚举项的文本信息
	 *
	 * @param code
	 * @return
	 */
	public static String getEnumText(String code) {
		for (PayerTypeEnum cur : PayerTypeEnum.values()) {
			if (cur.getCode().equals(code)) {
				return cur.text;
			}
		}
		return "未知";//PayerTypeEnum.UnKnown.text;
	}

	/*========================================判断是否相等========================================*/

	/**
	 * 判断两个枚举是否相等
	 *
	 * @param target
	 * @return
	 */
	public boolean equals(PayerTypeEnum target) {
		if (target == null) return false;
		return this == target || this.code.equals(target.code);
	}

	/**
	 * 判断两个枚举是否相等
	 *
	 * @return
	 */
	public boolean equals(String targetCode) {
		if (targetCode == null) return false;
		return this.code.equals(targetCode);
	}

	/**
	 * 判断两个枚举是否相等
	 *
	 * @param left
	 * @param right
	 * @return
	 */
	public static boolean equals(PayerTypeEnum left, PayerTypeEnum right) {
		return left == right || left != null && right != null && left.code.equals(right.code);
	}

	/**
	 * 判断两个枚举是否相等
	 *
	 * @param left
	 * @param rightCode
	 * @return
	 */
	public static boolean equals(PayerTypeEnum left, String rightCode) {
		return left == null && rightCode == null || left != null && rightCode != null && left.code.equals(rightCode);
	}

	/*========================================判断是否在一个列表中========================================*/

	/**
	 * 判断一个枚举是否在一组枚举列表中
	 *
	 * @param targets
	 * @return
	 */
	public boolean in(PayerTypeEnum... targets) {
		if (targets == null || targets.length == 0) {
			return false;
		}
		for (PayerTypeEnum cur : targets) {
			if (cur == this || cur.code.equals(this.code)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 判断一个枚举是否在一组枚举列表中
	 *
	 * @param src
	 * @param targets
	 * @return
	 */
	public static boolean in(PayerTypeEnum src, PayerTypeEnum... targets) {
		if (src == null || targets == null || targets.length == 0) {
			if (src == null && (targets == null || targets.length == 0)) return true;
			return false;
		}
		for (PayerTypeEnum cur : targets) {
			if (cur == src || cur.code.equals(src.code)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 判断一个枚举是否在一组枚举列表中
	 *
	 * @param srcCode
	 * @param targets
	 * @return
	 */
	public static boolean in(String srcCode, PayerTypeEnum... targets) {
		if (srcCode == null || targets == null || targets.length == 0) {
			if (srcCode == null && (targets == null || targets.length == 0)) return true;
			return false;
		}
		for (PayerTypeEnum cur : targets) {
			if (cur.code.equals(srcCode)) {
				return true;
			}
		}
		return false;
	}

	public static List<Map> payerTypeList = new ArrayList<>();

	public static List<Map> getPayerTypeList() {
		for (PayerTypeEnum payerTypeEnum : PayerTypeEnum.values()) {
			Map<String, String> data = new HashMap<>();
			data.put("code", payerTypeEnum.getCode());
			data.put("desc", payerTypeEnum.getText());
			payerTypeList.add(data);
		}

		return payerTypeList;
	}

}
