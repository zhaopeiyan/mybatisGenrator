package com.ke.tools.finance.Enum.entity;

import java.util.Date;

public class payerTypeDict {
    private Integer id;

    private String payertypeCode;

    private String payertypeValue;

    private String payertypeName;

    private Date createDate;

    private Boolean isvalid;

    private String desc;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPayertypeCode() {
        return payertypeCode;
    }

    public void setPayertypeCode(String payertypeCode) {
        this.payertypeCode = payertypeCode;
    }

    public String getPayertypeValue() {
        return payertypeValue;
    }

    public void setPayertypeValue(String payertypeValue) {
        this.payertypeValue = payertypeValue;
    }

    public String getPayertypeName() {
        return payertypeName;
    }

    public void setPayertypeName(String payertypeName) {
        this.payertypeName = payertypeName;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Boolean getIsvalid() {
        return isvalid;
    }

    public void setIsvalid(Boolean isvalid) {
        this.isvalid = isvalid;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}