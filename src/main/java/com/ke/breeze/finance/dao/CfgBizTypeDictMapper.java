package com.ke.breeze.finance.dao;

import com.ke.breeze.finance.entity.CfgBizTypeDict;
import com.ke.breeze.finance.entity.CfgBizTypeDictManager;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

public interface CfgBizTypeDictMapper {
    long countByExample(CfgBizTypeDictManager example);

    int deleteByExample(CfgBizTypeDictManager example);

    int deleteByPrimaryKey(Integer id);

    int insert(CfgBizTypeDict record);

    int insertSelective(CfgBizTypeDict record);

    List<CfgBizTypeDict> selectByExampleWithRowbounds(CfgBizTypeDictManager example, RowBounds rowBounds);

    List<CfgBizTypeDict> selectByExample(CfgBizTypeDictManager example);

    CfgBizTypeDict selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") CfgBizTypeDict record, @Param("example") CfgBizTypeDictManager example);

    int updateByExample(@Param("record") CfgBizTypeDict record, @Param("example") CfgBizTypeDictManager example);

    int updateByPrimaryKeySelective(CfgBizTypeDict record);

    int updateByPrimaryKey(CfgBizTypeDict record);
}