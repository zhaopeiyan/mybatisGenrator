package com.ke.breeze.finance.dao;

import com.ke.breeze.finance.entity.AxtCompanyCodeConf;
import com.ke.breeze.finance.entity.AxtCompanyCodeConfManager;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

public interface AxtCompanyCodeConfMapper {
    long countByExample(AxtCompanyCodeConfManager example);

    int deleteByExample(AxtCompanyCodeConfManager example);

    int deleteByPrimaryKey(Long id);

    int insert(AxtCompanyCodeConf record);

    int insertSelective(AxtCompanyCodeConf record);

    List<AxtCompanyCodeConf> selectByExampleWithRowbounds(AxtCompanyCodeConfManager example, RowBounds rowBounds);

    List<AxtCompanyCodeConf> selectByExample(AxtCompanyCodeConfManager example);

    AxtCompanyCodeConf selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") AxtCompanyCodeConf record, @Param("example") AxtCompanyCodeConfManager example);

    int updateByExample(@Param("record") AxtCompanyCodeConf record, @Param("example") AxtCompanyCodeConfManager example);

    int updateByPrimaryKeySelective(AxtCompanyCodeConf record);

    int updateByPrimaryKey(AxtCompanyCodeConf record);
}