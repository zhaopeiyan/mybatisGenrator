package com.ke.breeze.finance.dao;

import com.ke.breeze.finance.entity.CfgRoleTypeDict;
import com.ke.breeze.finance.entity.CfgRoleTypeDictManager;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

public interface CfgRoleTypeDictMapper {
    long countByExample(CfgRoleTypeDictManager example);

    int deleteByExample(CfgRoleTypeDictManager example);

    int deleteByPrimaryKey(Long id);

    int insert(CfgRoleTypeDict record);

    int insertSelective(CfgRoleTypeDict record);

    List<CfgRoleTypeDict> selectByExampleWithRowbounds(CfgRoleTypeDictManager example, RowBounds rowBounds);

    List<CfgRoleTypeDict> selectByExample(CfgRoleTypeDictManager example);

    CfgRoleTypeDict selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") CfgRoleTypeDict record, @Param("example") CfgRoleTypeDictManager example);

    int updateByExample(@Param("record") CfgRoleTypeDict record, @Param("example") CfgRoleTypeDictManager example);

    int updateByPrimaryKeySelective(CfgRoleTypeDict record);

    int updateByPrimaryKey(CfgRoleTypeDict record);
}