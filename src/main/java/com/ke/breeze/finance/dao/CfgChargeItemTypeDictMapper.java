package com.ke.breeze.finance.dao;

import com.ke.breeze.finance.entity.CfgChargeItemTypeDict;
import com.ke.breeze.finance.entity.CfgChargeItemTypeDictManager;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

public interface CfgChargeItemTypeDictMapper {
    long countByExample(CfgChargeItemTypeDictManager example);

    int deleteByExample(CfgChargeItemTypeDictManager example);

    int deleteByPrimaryKey(Integer id);

    int insert(CfgChargeItemTypeDict record);

    int insertSelective(CfgChargeItemTypeDict record);

    List<CfgChargeItemTypeDict> selectByExampleWithRowbounds(CfgChargeItemTypeDictManager example, RowBounds rowBounds);

    List<CfgChargeItemTypeDict> selectByExample(CfgChargeItemTypeDictManager example);

    CfgChargeItemTypeDict selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") CfgChargeItemTypeDict record, @Param("example") CfgChargeItemTypeDictManager example);

    int updateByExample(@Param("record") CfgChargeItemTypeDict record, @Param("example") CfgChargeItemTypeDictManager example);

    int updateByPrimaryKeySelective(CfgChargeItemTypeDict record);

    int updateByPrimaryKey(CfgChargeItemTypeDict record);
}