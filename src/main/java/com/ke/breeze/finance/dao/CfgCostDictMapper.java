package com.ke.breeze.finance.dao;

import com.ke.breeze.finance.entity.CfgCostDict;
import com.ke.breeze.finance.entity.CfgCostDictManager;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

public interface CfgCostDictMapper {
    long countByExample(CfgCostDictManager example);

    int deleteByExample(CfgCostDictManager example);

    int deleteByPrimaryKey(Integer id);

    int insert(CfgCostDict record);

    int insertSelective(CfgCostDict record);

    List<CfgCostDict> selectByExampleWithRowbounds(CfgCostDictManager example, RowBounds rowBounds);

    List<CfgCostDict> selectByExample(CfgCostDictManager example);

    CfgCostDict selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") CfgCostDict record, @Param("example") CfgCostDictManager example);

    int updateByExample(@Param("record") CfgCostDict record, @Param("example") CfgCostDictManager example);

    int updateByPrimaryKeySelective(CfgCostDict record);

    int updateByPrimaryKey(CfgCostDict record);
}