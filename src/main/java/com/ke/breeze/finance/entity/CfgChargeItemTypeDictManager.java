package com.ke.breeze.finance.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CfgChargeItemTypeDictManager {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public CfgChargeItemTypeDictManager() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andChargeitemCodeIsNull() {
            addCriterion("chargeItem_code is null");
            return (Criteria) this;
        }

        public Criteria andChargeitemCodeIsNotNull() {
            addCriterion("chargeItem_code is not null");
            return (Criteria) this;
        }

        public Criteria andChargeitemCodeEqualTo(String value) {
            addCriterion("chargeItem_code =", value, "chargeitemCode");
            return (Criteria) this;
        }

        public Criteria andChargeitemCodeNotEqualTo(String value) {
            addCriterion("chargeItem_code <>", value, "chargeitemCode");
            return (Criteria) this;
        }

        public Criteria andChargeitemCodeGreaterThan(String value) {
            addCriterion("chargeItem_code >", value, "chargeitemCode");
            return (Criteria) this;
        }

        public Criteria andChargeitemCodeGreaterThanOrEqualTo(String value) {
            addCriterion("chargeItem_code >=", value, "chargeitemCode");
            return (Criteria) this;
        }

        public Criteria andChargeitemCodeLessThan(String value) {
            addCriterion("chargeItem_code <", value, "chargeitemCode");
            return (Criteria) this;
        }

        public Criteria andChargeitemCodeLessThanOrEqualTo(String value) {
            addCriterion("chargeItem_code <=", value, "chargeitemCode");
            return (Criteria) this;
        }

        public Criteria andChargeitemCodeLike(String value) {
            addCriterion("chargeItem_code like", value, "chargeitemCode");
            return (Criteria) this;
        }

        public Criteria andChargeitemCodeNotLike(String value) {
            addCriterion("chargeItem_code not like", value, "chargeitemCode");
            return (Criteria) this;
        }

        public Criteria andChargeitemCodeIn(List<String> values) {
            addCriterion("chargeItem_code in", values, "chargeitemCode");
            return (Criteria) this;
        }

        public Criteria andChargeitemCodeNotIn(List<String> values) {
            addCriterion("chargeItem_code not in", values, "chargeitemCode");
            return (Criteria) this;
        }

        public Criteria andChargeitemCodeBetween(String value1, String value2) {
            addCriterion("chargeItem_code between", value1, value2, "chargeitemCode");
            return (Criteria) this;
        }

        public Criteria andChargeitemCodeNotBetween(String value1, String value2) {
            addCriterion("chargeItem_code not between", value1, value2, "chargeitemCode");
            return (Criteria) this;
        }

        public Criteria andChargeitemNameIsNull() {
            addCriterion("chargeItem_name is null");
            return (Criteria) this;
        }

        public Criteria andChargeitemNameIsNotNull() {
            addCriterion("chargeItem_name is not null");
            return (Criteria) this;
        }

        public Criteria andChargeitemNameEqualTo(String value) {
            addCriterion("chargeItem_name =", value, "chargeitemName");
            return (Criteria) this;
        }

        public Criteria andChargeitemNameNotEqualTo(String value) {
            addCriterion("chargeItem_name <>", value, "chargeitemName");
            return (Criteria) this;
        }

        public Criteria andChargeitemNameGreaterThan(String value) {
            addCriterion("chargeItem_name >", value, "chargeitemName");
            return (Criteria) this;
        }

        public Criteria andChargeitemNameGreaterThanOrEqualTo(String value) {
            addCriterion("chargeItem_name >=", value, "chargeitemName");
            return (Criteria) this;
        }

        public Criteria andChargeitemNameLessThan(String value) {
            addCriterion("chargeItem_name <", value, "chargeitemName");
            return (Criteria) this;
        }

        public Criteria andChargeitemNameLessThanOrEqualTo(String value) {
            addCriterion("chargeItem_name <=", value, "chargeitemName");
            return (Criteria) this;
        }

        public Criteria andChargeitemNameLike(String value) {
            addCriterion("chargeItem_name like", value, "chargeitemName");
            return (Criteria) this;
        }

        public Criteria andChargeitemNameNotLike(String value) {
            addCriterion("chargeItem_name not like", value, "chargeitemName");
            return (Criteria) this;
        }

        public Criteria andChargeitemNameIn(List<String> values) {
            addCriterion("chargeItem_name in", values, "chargeitemName");
            return (Criteria) this;
        }

        public Criteria andChargeitemNameNotIn(List<String> values) {
            addCriterion("chargeItem_name not in", values, "chargeitemName");
            return (Criteria) this;
        }

        public Criteria andChargeitemNameBetween(String value1, String value2) {
            addCriterion("chargeItem_name between", value1, value2, "chargeitemName");
            return (Criteria) this;
        }

        public Criteria andChargeitemNameNotBetween(String value1, String value2) {
            addCriterion("chargeItem_name not between", value1, value2, "chargeitemName");
            return (Criteria) this;
        }

        public Criteria andMoneytypeCodeIsNull() {
            addCriterion("moneyType_code is null");
            return (Criteria) this;
        }

        public Criteria andMoneytypeCodeIsNotNull() {
            addCriterion("moneyType_code is not null");
            return (Criteria) this;
        }

        public Criteria andMoneytypeCodeEqualTo(String value) {
            addCriterion("moneyType_code =", value, "moneytypeCode");
            return (Criteria) this;
        }

        public Criteria andMoneytypeCodeNotEqualTo(String value) {
            addCriterion("moneyType_code <>", value, "moneytypeCode");
            return (Criteria) this;
        }

        public Criteria andMoneytypeCodeGreaterThan(String value) {
            addCriterion("moneyType_code >", value, "moneytypeCode");
            return (Criteria) this;
        }

        public Criteria andMoneytypeCodeGreaterThanOrEqualTo(String value) {
            addCriterion("moneyType_code >=", value, "moneytypeCode");
            return (Criteria) this;
        }

        public Criteria andMoneytypeCodeLessThan(String value) {
            addCriterion("moneyType_code <", value, "moneytypeCode");
            return (Criteria) this;
        }

        public Criteria andMoneytypeCodeLessThanOrEqualTo(String value) {
            addCriterion("moneyType_code <=", value, "moneytypeCode");
            return (Criteria) this;
        }

        public Criteria andMoneytypeCodeLike(String value) {
            addCriterion("moneyType_code like", value, "moneytypeCode");
            return (Criteria) this;
        }

        public Criteria andMoneytypeCodeNotLike(String value) {
            addCriterion("moneyType_code not like", value, "moneytypeCode");
            return (Criteria) this;
        }

        public Criteria andMoneytypeCodeIn(List<String> values) {
            addCriterion("moneyType_code in", values, "moneytypeCode");
            return (Criteria) this;
        }

        public Criteria andMoneytypeCodeNotIn(List<String> values) {
            addCriterion("moneyType_code not in", values, "moneytypeCode");
            return (Criteria) this;
        }

        public Criteria andMoneytypeCodeBetween(String value1, String value2) {
            addCriterion("moneyType_code between", value1, value2, "moneytypeCode");
            return (Criteria) this;
        }

        public Criteria andMoneytypeCodeNotBetween(String value1, String value2) {
            addCriterion("moneyType_code not between", value1, value2, "moneytypeCode");
            return (Criteria) this;
        }

        public Criteria andCreateDateIsNull() {
            addCriterion("create_date is null");
            return (Criteria) this;
        }

        public Criteria andCreateDateIsNotNull() {
            addCriterion("create_date is not null");
            return (Criteria) this;
        }

        public Criteria andCreateDateEqualTo(Date value) {
            addCriterion("create_date =", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateNotEqualTo(Date value) {
            addCriterion("create_date <>", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateGreaterThan(Date value) {
            addCriterion("create_date >", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateGreaterThanOrEqualTo(Date value) {
            addCriterion("create_date >=", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateLessThan(Date value) {
            addCriterion("create_date <", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateLessThanOrEqualTo(Date value) {
            addCriterion("create_date <=", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateIn(List<Date> values) {
            addCriterion("create_date in", values, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateNotIn(List<Date> values) {
            addCriterion("create_date not in", values, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateBetween(Date value1, Date value2) {
            addCriterion("create_date between", value1, value2, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateNotBetween(Date value1, Date value2) {
            addCriterion("create_date not between", value1, value2, "createDate");
            return (Criteria) this;
        }

        public Criteria andIsvalidIsNull() {
            addCriterion("isValid is null");
            return (Criteria) this;
        }

        public Criteria andIsvalidIsNotNull() {
            addCriterion("isValid is not null");
            return (Criteria) this;
        }

        public Criteria andIsvalidEqualTo(Boolean value) {
            addCriterion("isValid =", value, "isvalid");
            return (Criteria) this;
        }

        public Criteria andIsvalidNotEqualTo(Boolean value) {
            addCriterion("isValid <>", value, "isvalid");
            return (Criteria) this;
        }

        public Criteria andIsvalidGreaterThan(Boolean value) {
            addCriterion("isValid >", value, "isvalid");
            return (Criteria) this;
        }

        public Criteria andIsvalidGreaterThanOrEqualTo(Boolean value) {
            addCriterion("isValid >=", value, "isvalid");
            return (Criteria) this;
        }

        public Criteria andIsvalidLessThan(Boolean value) {
            addCriterion("isValid <", value, "isvalid");
            return (Criteria) this;
        }

        public Criteria andIsvalidLessThanOrEqualTo(Boolean value) {
            addCriterion("isValid <=", value, "isvalid");
            return (Criteria) this;
        }

        public Criteria andIsvalidIn(List<Boolean> values) {
            addCriterion("isValid in", values, "isvalid");
            return (Criteria) this;
        }

        public Criteria andIsvalidNotIn(List<Boolean> values) {
            addCriterion("isValid not in", values, "isvalid");
            return (Criteria) this;
        }

        public Criteria andIsvalidBetween(Boolean value1, Boolean value2) {
            addCriterion("isValid between", value1, value2, "isvalid");
            return (Criteria) this;
        }

        public Criteria andIsvalidNotBetween(Boolean value1, Boolean value2) {
            addCriterion("isValid not between", value1, value2, "isvalid");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}