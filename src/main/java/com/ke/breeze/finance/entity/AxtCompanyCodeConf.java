package com.ke.breeze.finance.entity;

import java.util.Date;

public class AxtCompanyCodeConf {
    private Long id;

    private String companyCode;

    private Date effectiveTime;

    private Date cretedTime;

    private Date updatedTime;

    private String createdBy;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public Date getEffectiveTime() {
        return effectiveTime;
    }

    public void setEffectiveTime(Date effectiveTime) {
        this.effectiveTime = effectiveTime;
    }

    public Date getCretedTime() {
        return cretedTime;
    }

    public void setCretedTime(Date cretedTime) {
        this.cretedTime = cretedTime;
    }

    public Date getUpdatedTime() {
        return updatedTime;
    }

    public void setUpdatedTime(Date updatedTime) {
        this.updatedTime = updatedTime;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }
}